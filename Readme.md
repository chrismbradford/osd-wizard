# What is OSD Wizard?
OSD Wizard is a PowerShell script with a WPF/ XAML-based UI built for use within System Center Configuration Manager (SCCM / ConfigMgr) Task Sequences - it has two key purposes, the second of which is optional:
1. Reduce "human error" factors during early stages of Operating System Deployment
2. Enable location (network) derived automation for multi-language environments

More information on both of these features, and how you can leverage them below.

![OSD Wizard Screenshot](https://i.imgur.com/PhD3AZL.png)

The script works in both Bare Metal (PXE-boot) and in-place Wipe and Reload (Refresh) scenarios, both of which can be completed from a single Task Sequence. 
You can also use the script during in-place upgrades/ architecture changes.

The user interface itself provides basic capabilities you would expect:
* Abort the Task Sequence, early on, in the event of the wrong Task Sequence being executed on the device (avoiding data loss*)
* Specify a Computer Name / validate the name in-line with RFC / warns user of invalid or duplicate Computer Name in target Domain
* Select / override a Location (from a pre-defined list, based upon networks), each of which can contain specific regional/ language settings
* Select a target Organisational Unit from target Domain / move existing Computer account to desired target Organisational Unit
* Specify a Description for use on the Computer account in Active Directory
* Specify Primary User(s) for the device, validating that the supplied accounts actually exist
*\*Your data is your responsibility, always have a backup*

The real "power" of the tool though is the use of a Web Service (Maik Koster's Deployment Web Service) and the XML files "behind the scenes" (used to minimise Web Service calls), **enabling this script to function well in dispersed, high latency, low-bandwidth networks.**

Of course, none of this would work without ConfigMgr Task Sequence variables.

## "Human Error Factors" ?!
When I first started working with SCCM/ ConfigMgr a few years ago I ran into several issues with upstream teams trying to deploy Task Sequences to devices that were untested (i.e. there were no drivers), power was not plugged in, the user supplied hostname was duplicated in target domain etc. This generated unnecessary Incidents, and delays in getting devices out to users. I became increasingly frustrated at the lack of ConfigMgr built-in capabilities to address these issues, so I turned to vbScript to write a HTA to address these issues. vbScript is now, several years later, a dying technology, so over time I have re-written the vbScript/ HTA in PowerShell, leveraging WPF and XAML to generate a user interface.

Using OSD Wizard in your Task Sequences you will enable the following pre-Task Sequence validation to take place:
* **Task Sequence verification** - based on the name of the Task Sequence itself, validates OSD Wizard integration has been tested by the Systems Administrator
* **Platform verification** - based on CMI/ WMI obtained Manufacturer, Model, BIOS and UEFI status information and compared against pre-defined rules, if there is a match the user can proceed, if there is no match the user cannot proceed with the Task Sequence, avoiding an eventual failure anyway
* **Mains Power is connected, if Laptop or Tablet platform** - if not, the user is prompted to connect it before they can proceed, avoiding a device running out of power mid-Task Sequence
* **Supplied Computer name is not already in use** - if duplicate detected user is prompted to either change the supplied name or reset the account and enable an override flag - avoiding Domain Join failing during the Task Sequence
* **Target Organisation Unit matches existing Computer account** - if not user is prompted, they can either change the selected OU, or check a box to move the existing account to the desired target - avoiding incorrect OU placement for the Computer account
* **Supplied Primary Users (for UDA) exist** - if not user is asked to remediate supplied user names in format DOMAIN\username - avoiding incorrect/ missing Primary Users

In effect, you are in control of what can be built and what can't, instead of any device being able to run a Task Sequence, irrespective of driver packs etc. being available to make sure the deployment is successful. This is particularly useful when running desktop refresh programs (ensuring legacy devices cannot be rebuilt) or user-driven in-place upgrade programs.

## "Location-derived automation for multi-language environments" ?!
When originally designing this script (or its vbScript predecessor) I had to "think global" - many solutions used lots of MDT/ ConfigMgr database calls or relied heavily upon web service calls, both of these (as tested!) perform terribly over high-latency, wide area networks. To avoid this issue, OSD Wizard predominately uses XML files, exported from a web service, included **in the package** alongside the script itself. There are a few exceptions - namely, checking a user or computer exists (new accounts are created often/ frequently), moving a computer to a new OU and setting the description in Active Directory for a Computer Object. All of these are "online only" capabilities.

So, "Locations" ?
> A key concept here is that a **Location is defined by its IPv4 gateway(s)** that would be detected either in Widows PE / PXE or production Operating System environments. Any one location can have multiple gateways, but a single IPv4 gateway address should not* exist in more than one Location.

*\*Note: duplicating a gateway address will not break anything, just the script will automatically pick the lowest numbered location and populate settings from this location. You could, therefore, use locations as "departments" or functions to leverage the automatic OU selection capability, whilst configuring like-for-like language settings for each department.*

Whilst for small companies/ environments you may only need a single location, for environments where there are a large number of locations (tens, hundreds or more!) I would suggest that you group locations, by Operating Company, business unit or similar, creating "super locations" that contain required locale settings, but the user has to select the desired Organisational Unit.

Based on the detected gateway, and thus Location, at load the script will automatically discover, as defined in Locations.xml:
* Computer name syntax (option to specify location prefix, plus prefix based on platform type)
* Target Domain
* Target Organisational Unit
* TimeZone
* Operating system UILanguage, UIFallback language, UserLocale, InputLocale

You can override the identified location, or some of the specific/ individual settings above (namely computer name, target Organisational Unit).

## Modes
OSD Wizard offers four "modes" at run time, if the "mode" command line argument is not supplied at execution OSD Wizard will automatically run in "Default" mode:
* **Default** - use at the start of any Bare Metal or Refresh Task Sequence
* **SetDescription** - use mid- Bare Metal Task Sequence to set the Computer Account description in Active Directory via the WebService
* **ExportOU** - use in the preparation of your environment to obtain XML OU exports for Active Directory domains in your environment
* **Test** - use outside of a Task Sequence to validate form layout, locations, OUs, platform validation (more information on testing capabilities/ limitations below)

# Installation
Within this repository you will find:
* **OSDWizard_SCCM.ps1** - the PowerShell script that does the "heavy lifting"
* **TaskSequences.xml** - example list of Task Sequences, and related settings, for use with OSD Wizard
* **Config.xml** - global settings used throughout OSDWizard_SCCM.ps1 
* **Locations.xml** - example list of locations, and individual settings, based upon IPv4 gateway address
* **Unattend.xml** - example Windows 10 x64/ x86 1703 Unatended file
* **cb-net-co-uk-OUs.xml** - example OU export from Maik Koster's Deployment Web Service, use 'ExportOU' mode to generate this for your domain(s)
* **Win10.xml** - example platform validation XML for use with Windows 10
* **Win2012R2.xml** - example platform validation XML for use with Windows 2012R2
* **Win81.xml** - example platform validation XML for use with Windows 8.1
* **ServiceUIx64.exe** - part of MDT, used to display OSD Wizard during in-place wipe/ reload/ Refresh\*
* **ServiceUIx86.exe** - part of MDT, used to display OSD Wizard during in-place wipe/ reload/ Refresh\*
* **Win10 BM+Refresh.zip** - an example Task Sequence, exported from ConfigMgr Current Branch, configured to leverage the capabilities of OSD Wizard

*\*Note that architecture-specific ServiceUI.exe executables are required, otherwise OSD Wizard will be unable to leverage the Microsoft.SMS.TSEnvironment COM Object to both read and write Task Sequence variables.*

## Pre-Requisites/ Configuration
* Install and configure a copy of Maik Koster Deployment Tools Web Service (available here: http://maikkoster.com/deployment-web-service-7-3-sccm-client-center-support/ )\*. Ensure this is running under an Application Pool with an identity that has the following permissions in **ALL** of the domains you wish to deploy devices to:
    * Create, Delete Computer Objects (needed to move devices between OUs)
    * Write All Properties on Computer Objects

    *Web service download mirror: http://www.cb-net.co.uk/wp-content/uploads/2017/09/MaikKoster.Deployment.Webservice_v7_3.zip*

* Create an OU extract for ALL domains you wish to use OSD Wizard with, run OSD Wizard from a PowerShell terminal as below:
    * **.\OSDWizard_SCCM.ps1 -mode ExportOU -ouexportdomain \<fqdn of domain\>**

    For example the command: **.\OSDWizard_SCCM.ps1 -mode ExportOU -ouexportdomain cb-net.co.uk** would create an XML export of all OUs for the domain 'cb-net.co.uk' - named as OSD Wizard expects, within the same directory as the OSD Wizard script.

    Ensure you have installed/ configured the Web Service prior to running OSD Wizard in 'ExportOU' mode.

### OSD Wizard Environment/ Configuration Files
**All of the files you author below must be included in the OSD Wizard Package you will create in ConfigMgr.**

* **Locations.xml** - a single location example is included below, check the supplied Locations.xml file for a multi-location example. The defined locations should include any location, with its localised configuration, from which you want to use OSD Wizard in a Task Sequence. Note that for any AD Domain specified under "JoinDomain" you must ensure the OSD Wizard package includes an OU export XML file.
 
~~~XML
<?xml version="1.0" encoding="utf-8"?>
<ArrayOfLocationSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://cb-net.co.uk/Deployment">
	<LocationSettings>
		<!-- France, UK-based support team so en-GB keyboard mapping also available -->
		<Location id="1" name="Paris, France">
			<Settings> 
				  <!-- Keyboard Mappings Deployed, Prioritised -->
				  <InputLocale>fr-FR;en-GB</InputLocale>
				  <!-- Used within Task Sequence for Language Pack installation -->
				  <SecondaryUILanguage>fr-FR</SecondaryUILanguage>
				  <!-- Default Input/Keyboard/Regional Language -->
				  <UserLocale>fr-FR</UserLocale>
				  <!-- Primary Display Language -->
				  <UILanguage>fr-FR</UILanguage>
				  <!-- System Fall-back UILanguage -->
				  <UIFallbackLanguage>en-US</UIFallbackLanguage>
				  <TimeZoneName>Romance Standard Time</TimeZoneName>
				  <JoinDomain>cb-net.co.uk</JoinDomain>
				  <LocationDefaultOU>OU=Paris,OU=France,OU=Sites,DC=cb-net,DC=co,DC=uk</LocationDefaultOU>
				  <SitePrefix>FR</SitePrefix>
				  <LaptopPrefix>-L-</LaptopPrefix>
				  <WorkstationPrefix>-W-</WorkstationPrefix>
				  <TabletPrefix>-L-</TabletPrefix>
				  <DefaultGateway>10.33.0.254</DefaultGateway>
			</Settings>
		</Location>
	</LocationSettings>
</ArrayOfLocationSettings>
~~~


* **TaskSequences.xml** - single Windows 10 Task Sequence example is included below for x86 and x64 image deployment, defaulting to x64. Check the supplied TaskSequences.xml for a multi-Task Sequence example. Note that **the "Name" field must match, like for like, the Task Sequence name as defined in the ConfigMgr console.**

```XML
<?xml version="1.0" encoding="utf-8"?>
<TaskSeqeunces xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://cb-net.co.uk/Deployment">
	<TaskSequence Name="Deploy or Refresh Windows 10 [B/M + REFRESH]" chkPlatformValid="TRUE" chkPlatformXML="Win10.xml">
		<option osName="Windows 10 Enterprise x64" osString="Win10x64" default="TRUE" isX64="TRUE"/>
		<option osName="Windows 10 Enterprise x86" osString="Win10x86" default="FALSE" isX64="FALSE"/>
	</TaskSequence>
</TaskSeqeunces>
```


* **Platform Validation Rules** - you can group these by OS, or into a single file and use this against all Task Sequences where OSD Wizard is called. I have found that per-OS works well, especially as vendors/ MS will not support older devices on Windows 10 for example. However you proceed, the name of the file must be entered into the "chkPlatformXML" property for the Task Sequence in TaskSequences.xml. The example below would validate a couple of Dell and Microsoft devices. View the supplied "Win10.xml" file for a larger set of examples. **Anything not on this list will fail platform validation.**

```XML
<?xml version="1.0" encoding="utf-8"?>
<ArrayOfDeploymentRules xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://cb-net.co.uk/Deployment">
  <DeploymentRules>
    <Vendor name="Dell Inc.">
      <Platform model="Latitude E7240" MinSupportedBios="A07" UEFISupport="UEFI" type="isLaptop" />
      <Platform model="Latitude E7440" MinSupportedBios="A07" UEFISupport="UEFI" type="isLaptop" />
    </Vendor>
    <Vendor name="Microsoft Corporation">
      <Platform model="Surface Pro 2" MinSupportedBios="ANY" UEFISupport="UEFI" type="isTablet" /> 
      <Platform model="Virtual Machine" MinSupportedBios="ANY" UEFISupport="ANY" type="isVM" /> 
    </Vendor>
  </DeploymentRules>
 </ArrayOfDeploymentRules>
```


* **Review Config.xml** - some of the settings may/ may not be desirable in your environment, I have included the default contents/ settings below:

```XML
<?xml version="1.0" encoding="utf-8"?>
<Settings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://cb-net.co.uk/Deployment">
    <!-- ################################# General Settings ############################################################## -->
    <!-- When 'True' enables console *and* log-file based output -->
    <debug>True</debug>
    <!-- Log-file output for when $debug set to True -->
    <logfile>%TEMP%\OSDWizard.log</logfile>
    <!-- Environment-specific loctaions based upon IP gateway  -->
    <locationsXML>Locations.xml</locationsXML>
    <!-- Environment-specific, list of Task Sequences configured to support this script -->
    <tasksequenceXML>TaskSequences.xml</tasksequenceXML>
    <!-- When 'True' ensure that like-for-like O/S (32/64-bit) is imaged during refresh, overides default image for Task Sequence -->
    <sameArchRefresh>True</sameArchRefresh>
    <!-- When 'True' during Refresh Scenario will select *existing* OU on load/ location change (overriding default OU per location) -->
    <!-- Functions only when existing and target domain are the same, user can still override target OU -->
    <sameOURefresh>True</sameOURefresh>
    <!-- If set to True, enables auto-generation of computer account names based on BIOS asset tag and prefix(es) supplied in Locations.xml -->
    <generateComputerName>True</generateComputerName>
    <!-- URL for Maik Koster's Deployment Web Service, used for computer name/ user name / OU checking moving computer objects in active Directory  -->
    <ADwebserviceURL>http://cmwebsvc.cb-net.co.uk/cmwebsvc/adex.asmx?WSDL</ADwebserviceURL>
    <!-- Currently unused, for use with future WebService such as https://www.scconfigmgr.com/2017/05/23/configmgr-webservice-version-1-3-0-released/  -->
    <ADwebserviceKey></ADwebserviceKey>
    <!-- Used during 'ExportOU' mode only, defines the levels of sub/child-OUs captured during export from Web Service -->
    <ExportOULevel>5</ExportOULevel>
    <!-- ################################# Test Mode Settings ############################################################## -->
    <!-- Spoofed Task Sequence name and scenario mode -->
    <TSTtsName>Deploy or Refresh Windows 10 [B/M + REFRESH]</TSTtsName>
    <TSTisRefresh>False</TSTisRefresh>
    <!-- Platform spoofing configuration for validation rule testing against chkplatformXML -->
    <TSTspoofPlatform>True</TSTspoofPlatform>
    <!-- All of the below are optional, if not supplied 'Test' mode will use platform detected values -->
    <TSTcomputerName>VWINCLIENT2</TSTcomputerName>
    <TSTcomputerDomain>cb-net.co.uk</TSTcomputerDomain>
    <TSTplatformManufacturer>Dell Corporation</TSTplatformManufacturer>
    <TSTplatformModel>Latitude E7240</TSTplatformModel>
    <TSTplatformBIOS>A12</TSTplatformBIOS>
    <TSTplatformAssetTag>FM4Z9S1</TSTplatformAssetTag>
    <TSTplatformUEFIstate>True</TSTplatformUEFIstate>
</Settings>
```


### ConfigMgr Configuration
* Configure your ConfigMgr Boot Images to include the following Optional Components:
    * WinPE-HTA
    * WinPE-NetFX
    * WinPE-PowerShell
* Configure your PXE service Point to "Allow UDA with auto-approval" : https://blogs.technet.microsoft.com/inside_osd/2011/06/20/configuration-manager-2012-user-device-affinity-and-os-deployment/ (**Note** for UDA to actually work you must discover both Computer *and* User objects in your environment via ConfigMgr)
* Package creation:
    * If you plan to use the multi-language capabilities, **create OS and architecture-specific Language Pack packages**, for Windows 10 see https://blogs.technet.microsoft.com/mniehaus/2017/04/26/finding-windows-10-language-packs/:
        * In the package root create per-language folders then, copy the .cab file from the language pack media and rename it "lp.cab" - logically, this would look like:
        * \ (package root folder)
            * \fr-FR\lp.cab
            * \de-DE\lp.cab
            * \es-ES\lp.cab
    * Create a new Package in ConfigMgr named "OSD Wizard" ensure you have included **all** of the files in this repository, overwriting included files with files you have created for your environment - these should include:
        * Locations.xml
        * TaskSequences.xml
        * Config.xml
        * *PlatformValidationFileName*.xml
        * *\<domainName\>*-OUs.xml (generated via 'ExportOU' mode)
        * Unattend.xml
    * Ensure you distribute the content for this package to all DPs where clients will use OSD Wizard
    * Ensure that all Clients that use OSD Wizard can access the Deployment Tools Web Service instance
* Import and review the supplied Task Sequence, modify to suit your environment/ needs
    * Use the supplied Unattend file in this package (or take key elements from it if not using with Windows 10 1703)
* Ensure your Domain Join account is configured with the correct permissions in **all** Active Directory Domains you wish to use OSD Wizard: http://blog.ctglobalservices.com/configuration-manager-sccm/mip/creating-a-joindomain-account-for-use-with-sccm-osd/
* *Finally*, ensure you setup a regular update of the XML file(s) containing the OU export within the OSD Wizard package - this can be automated by calling OSD Wizard in 'ExportOU' mode for each domain, then automating the update of the SCCM/ ConfigMgr package on your Distribution Points either by setting an update schedule on the package itself, or using PowerShell.

### Testing
You can test OSD Wizard on a specific device by using 'test' mode, ensure Config.xml contains desired values for 'TSTisRefresh' (used to spoof a Bare-Metal or Wipe and Reload scenario) and 'TSTtsName' (used to spoof Task Sequence name):
* **PowerShell.exe -ExecutionPolicy Bypass .\OSDWizard_SCCM.ps1 -mode 'Test'**

In "Test" mode OSD Wizard will:
* Render the UI, testing supplied XAML, form layout
* Detect platform information
* Populate locations
* Detect device location
* Populate OUs for Detected Location
* Populate Operating System Images for detected Task Sequence
* Generate computer name based on Locations.xml prefixes/ global config flag
* Perform platform validation against detected platform/ spoofed Task Sequence name

*You cannot test Task Sequence variables as the needed COM Object will not exist outside of a Task Sequence and, under Test mode the Web Service is not made available.*

You can also spoof platform information under 'Test' mode - ensure the Config.xml setting 'TSTspoofPlatform' is set to True to enable this. Override values are also defined in Config.xml, enabling you to test wbemtest.exe obtained values prior to Task Sequence testing under WinPE.

'Test' mode saves you having to run OSD wizard endlessly via F8/ a Command Prompt during a Task Sequence to perform basic functionality testing.

**Note that whilst in 'test' mode, Web Service calls to Move Computer Account Organisational Unit and Set Descriptions will not execute.**

# Additional Information

## Setting up the Deployment Web Service
1. Download the Deployment Web Service using either the offical link, or mirror:
    * **Official:** http://maikkoster.com/deployment-web-service-7-3-sccm-client-center-support
    * **Mirror:** http://www.cb-net.co.uk/wp-content/uploads/2017/09/MaikKoster.Deployment.Webservice_v7_3.zip
2. Create a Active Directory Service Account that the Deployment Web Service Application Pool will RunAs, i.e. svc_cmwebsvc
3. Create a new, .NET 4, Integrated, Application Pool on the target IIS server, under Advanced Settings change the Identity to the new Service Account
4. Create a folder on the target IIS server, i.e. X:\www\cmwebsvc
5. Extract the Deployment Web Service archive to the folder created above
6. Create a new Application under the default WebSite on the target IIS server, i.e. cmwebsvc, with the Physical Path set as  the folder containing the extracted Deployment Web Service files
7. Test the Deployment Web Service instance:
    * Browse to http://\<**fqdnOfIISServer**\>/cmwebsvc/adex.asmx

If you encounter any issues with the Web Service, review generated logs under the Application Root\logs folder.

This Web Service must be accessible from any *client* device that will run OSD Wizard.

## Identifying Hardware Platform Validation Strings

You can use **wbemtest.exe** to identify Manufacturer string, model string, BIOS string for any new device you wish to add support for. This tool is available within Windows PE as well as a full-fat production Operating System. Enable command prompt support on a Windows PE, then hit F8 and enter wbemtest.exe, from full-fat Windows Start > Run > wbemtest.exe.

![WBEMTest Screenshot #1](https://i.imgur.com/gOnSsfa.png)
![WBEMTest Screenshot #2](https://i.imgur.com/3uG9SAY.png)
![WBEMTest Screenshot #3](https://i.imgur.com/WQMDvDj.png)
![WBEMTest Screenshot #4](https://i.imgur.com/S6Xx2NN.png)
![WBEMTest Screenshot #5](https://i.imgur.com/7MZU1XS.png)
![WBEMTest Screenshot #6](https://i.imgur.com/EyEJjl3.png)

* Query for Manufacturer string, model string: **select \* from Win32_ComputerSystem** (look for Manufacturer, Model)
* Query for BIOS version string: **select \* from Win32_BIOS** (look for SMBIOSBIOSVersion)

Note that UEFI configuration/ use is dependant upon several factors:
* The device(s) 
* The Task Sequence (have you configured/ enabled UEFI partitioning in the T/S)
* Your environments DHCP configuration (configuration of additional IP Helper addresses that point to you ConfigMgr PXE servers, after those that point to your DHCP servers)
 
Detection of the platforms UEFI status is via a ConfigMgr Task Sequence variable that is automatically generated (_SMSTSBootUEFI).

More information on required DHCP configuration for legacy BIOS and UEFI co-existence here: https://www.cb-net.co.uk/microsoft-articles/configmgr/configmgr-pxe-booting-legacy-bios-and-uefi/

# Troubleshooting
On execution a log file "OSDWizard.log" is created under %TEMP% - this is X:\Windows\TEMP under WinPE and C:\Windows\Temp under full-fat O/S.