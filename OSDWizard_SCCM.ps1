<###############################
#.SYNOPSIS
    This script generates a WPF/ XAML based form for use in ConfigMgr Task Sequences - it is suitable for use during both WinPE Bare-Metal and Production-OS Refresh 
    (in-place wipe and re-load) scenarios.
    The primary purpose of this script is to improve consistency and reduce "human error" during the early stages of Operating System Deployment, thus maximising OSD success rates.
    
#.DESCRIPTION
    This script performs validation/ automation relating to the below. In the event of pre-flight validation failing, data/ disks *should* remain untouched.
    Note that the author of this script is in *no way* responsible for data loss.
        > Hardware/ platform support for specific Task Sequence - administrator-defined Manufacturer, Model, BIOS version and UEFI status
        > Mains power status for laptop and tablet devices - will not allow user to continue until mains power is detected
        > Computer name generation/ input - validates length, use of illegal characters against RFC
        > Validation that the supplied Computer name is not already in use in target Active Directory domain (this would cause Domain Join, and thus Task Sequence, to fail)
        > Pre-defined "location" (detected network) based deployment scenarios - based on detected IP gateway - that automatically set:
            > Target Active Directory domain
            > Target/ default organisational unit (with option to retain existing OU, flag configurable in Config.xml)
            > Multi-language options including display language and input locales
            > Automatic host-name generation based upon location/ site prefix as well as desired platform type (Desktop, Laptop, Tablet) prefix
        > Ability to override automatically detected location (i.e. build for a location other than that identified based upon detected network)
        > Ability to override/ set target Organisational Unit for location-specific domain
        > Ability to move Computer Account Organisational Unit (within same Active Directory domain) before OSD commences
        > Ability to set Computer Account Description in Active Directory
            > During Refresh this uses a WebService call, from the WPF/PowerShell script
            > During Bare Metal this uses a vbScript within the Task Sequence itself
        > Primary User assignment; validates the user exists in Active Directory, assigns them as Primary User in ConfigMgr using SMSTSUdaUsers Task Sequence variable

    The script relies heavily on package-included XML files, this is by design in order to reduce CM database/ Web Service calls which are latency-sensitive. As a result this script 
    performs (very) well over global, high latency networks.
    You will have to ensure you maintain and update these XML files on a regular basis otherwise those that use the script will complain of missing OU's, locations etc.

    I have avoided any application-deployment capabilities - my opinion here (however contentious) is that you either bake-in, add to your Task Sequence or deploy to the user, not 
    device. By assigning a Primary User via this interface you are in a great place for the latter to automatically kick-in after OSD.

    To use this script you will require:
        > x64 and x86 PXE Boot Images that include optional components: HTML, Microsoft .NET, PowerShell
        > A modified version of Config.xml - you *must* supply a valid URL for the Deployment Web Service
        > A modified Locations.xml file (example included, modify for your network/ environment)
        > A modified TaskSequences.xml file (example included), modify to ensure this script is only used in conjunction with a compatible Task Sequences
        > A modified Win<ver>.xml file, (i.e. Win10.xml) (example included) - used for hardware validation on a per O/S basis, ensuring users do not try to deploy to untested 
            equipment and report failures
        > Maik Koster's Deployment Tools Web Service (link below), running under an application pool who's identity has Create, Delete and Write All Properties on Computer Accounts 
            in relevant OUs
        > Web-service generated OU export for each domain, renamed to replace "." with "-" and ending in "OUs.xml" - example included
        > A compatible Unattended file, (example included for Windows 10 1703 x86 and x64)
        > A Task Sequence that is setup to use this script and its resulting capabilities (example included)
        > PXE service point to be configured to support automatic Primary User assignment
        > ServiceUI (BOTH x86 and x64 versions - renamed ServiceUIx86.exe and ServiceUIx64.exe) available as part of MDT here: 
            > https://technet.microsoft.com/en-gb/windows/dn475741.aspx?f=255&MSPPError=-2147217396
        > If you are using the multi-language capabilities of this script ensure you download the correct version of the language packs, for Windows 10 see here
            > https://blogs.technet.microsoft.com/mniehaus/2017/04/26/finding-windows-10-language-packs/
            > You have to rename cab files to "lp.cab" and place in per-language folders within the package root
                > Package root "\"
                    > fr-FR\lp.cab
                    > es-ES\lp.cab
                    > de-DE\lp.cab
                    > en-GB\lp.cab
                    > etc      

#.EXAMPLE
    When using this script during a "Refresh" scenario (launched from a production/ running Windows Operating System) use the following command, alongside the 64-bit version of ServiceUI.exe - available in MDT - included in this package:
        > Run Command Line: ServiceUI.exe -process:TSProgressUI.exe %SYSTEMROOT%\System32\WindowsPowerShell\v1.0\powershell.exe -NoProfile -WindowStyle Hidden -ExecutionPolicy Bypass -File OSDWizard_SCCM.ps1
   
    When running this script during a Bare Metal scenario (launched form Windows PE) use the following configuration:
        > Run PowerShell Script: OSDWizard_SCCM.ps1
        (set Execution Policy to Bypass)
    
    When using this script mid-Task Sequence to set Computer Account Description, use the foolowing configuration:
        > Run PowerShell Script: OSDWizard_SCCM.ps1 -mode SetDescription
        (set Execution Policy to Bypass)

    You can use this script in 'ExportOU' mode to create Web Service generated XML Organisational Unit exports for your domains, an example command is shown below:
        > .\OSDWizard_SCCM.ps1 -mode ExportOU -ouexportdomain cb-net.co.uk
    
    When testing this script, outside of a Task Sequence, use the following command from a PowerShell terminal, replacing variables relating to 'Test' mode in Config,xml: 
        > .\OSDWizard_SCCM.ps1 -mode Test
    Testing enables you to validate form layout and that locations, OUs, Operating Systems populate, as well as platform validation (by running OSD Wizard in test mode on a specific model/ device).
    
    An example/ exported Task Sequence is included with this script/ associated files - I **strongly** suggest you use this as a starting point.

    To add your own location-specific setting(s) that you want to use within Task Sequence logic you need to:
        > Modify Locations.xml
        > Modify Get-LocationSettings
        > Modify Set-TSVars
        > Configure your Task Sequence accordingly

#.PARAMETER mode
    Either "Default", "SetDescription" or "Test", automatically switches to "Default" if $mode parameter not supplied
        > Default - for use at the start of Operating System Deployment/ within a Task Sequence only
        > SetDescription - for use mid-Task Sequence, calls Web Service to set AD Computer Account description attribute *only*
        > ExportOU - use alongside ouexportdomain parameter to create XML Organistaional Unit exports for use with OSD Wizard
        > Test - use for testing OSD Wizard outside of a ConfigMgr / SCCM Task Sequence
            > Also expects $testIsRefresh, $testTaskSequenceName as detailed below

#.PARAMETER ouexportdomain
    Used only with 'ExportOU' mode - the FQDN of the Domain you wish to create an Organisational Unit export for using the Web Service

#.NOTES
	-Author: Chris Bradford
	-Email: chris@cb-net.co.uk
	-CreationDate: 2017/09/04
	-LastModifiedDate: 2017/09/17
	-Version: 0.96-stable

#.LINK
    https://www.cb-net.co.uk

#.LINK
    http://maikkoster.com/deployment-web-service-7-3-sccm-client-center-support/

#.LINK
    https://technet.microsoft.com/en-gb/windows/dn475741.aspx?f=255&MSPPError=-2147217396
    
##############################
#>

# Script Command-line parameters, "mode" defaults to "Default"
[CmdletBinding()]
Param(
    [Parameter(Mandatory=$False)]
    [ValidateSet("Default","SetDescription","ExportOU","Test")]
    [String]
    $mode = "Default",
    [Parameter(Mandatory=$False)]
    [string]
    $ouexportdomain
)

# Load pre-requisite for WPF
[void][System.Reflection.Assembly]::LoadWithPartialName('presentationframework')

# Form XAML code, generated in Visual Studio
$inputXML = @"
<Window x:Class="OSDWizard.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:OSDWizard"
        mc:Ignorable="d"
        Title="O/S Deployment Wizard" Height="450" Width="535"
        WindowStartupLocation="CenterScreen">
    <Grid>
        <ComboBox x:Name="cmbTargetLocation" HorizontalAlignment="Left" Margin="195,162,0,0" VerticalAlignment="Top" Width="312" RenderTransformOrigin="0.5,0.5" FontSize="11" Grid.ColumnSpan="2"/>
        <Border BorderBrush="Black" BorderThickness="1" HorizontalAlignment="Left" Height="1" Margin="10,93,0,0" VerticalAlignment="Top" Width="499" Grid.ColumnSpan="2"/>
        <Label x:Name="lblFormTitle" Content="O/S Deployment Wizard" HorizontalAlignment="Left" Height="62" Margin="10,16,0,0" VerticalAlignment="Top" Width="499" FontSize="36" FontWeight="Bold" Background="#FF5A80B6" Foreground="White"/>
        <Label x:Name="lblTargetLocation" Content="Target Location:" HorizontalAlignment="Left" Margin="10,159,0,0" VerticalAlignment="Top" Width="164" Height="27" FontSize="11"/>
        <ComboBox x:Name="cmbTargetOU" HorizontalAlignment="Left" Margin="195,219,0,0" VerticalAlignment="Top" Width="312" FontSize="11" Grid.ColumnSpan="2"/>
        <Label x:Name="lblTargetOU" Content="Target Organisational Unit:" HorizontalAlignment="Left" Margin="10,213,0,0" VerticalAlignment="Top" Width="164" Height="27" FontSize="11"/>
        <TextBox x:Name="txtComputerName" HorizontalAlignment="Left" Height="23" Margin="195,107,0,0" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="312" FontSize="11" Grid.ColumnSpan="2"/>
        <Label x:Name="lblComputerName" Content="Computer Name:" HorizontalAlignment="Left" Margin="10,103,0,0" VerticalAlignment="Top" Width="164" Height="27" FontSize="11"/>
        <TextBox x:Name="txtTargetDomain" HorizontalAlignment="Left" Height="23" Margin="195,190,0,0" TextWrapping="Wrap" Text="TargetDomain" VerticalAlignment="Top" Width="312" RenderTransformOrigin="0.5,0.5" FontSize="11" Grid.ColumnSpan="2" IsEnabled="False"/>
        <Label x:Name="lblTargetDomain" Content="Target Domain:" HorizontalAlignment="Left" Margin="10,186,0,0" VerticalAlignment="Top" Width="164" Height="27" FontSize="11"/>
        <RadioButton x:Name="rdoAccountResetNo" Content="No" HorizontalAlignment="Left" Margin="473,139,0,0" VerticalAlignment="Top" FontSize="10"/>
        <RadioButton x:Name="rdoAccountResetYes" Content="Yes" HorizontalAlignment="Left" Margin="430,139,0,0" VerticalAlignment="Top" FontSize="10"/>
        <Label x:Name="lblExistingAccountReset" Content="Existing Account Reset:" HorizontalAlignment="Left" Height="29" Margin="316,133,0,0" VerticalAlignment="Top" Width="109" FontSize="10"/>
        <CheckBox x:Name="chkMoveComputer" Content="Move Computer Object" HorizontalAlignment="Left" Margin="381,250,0,0" VerticalAlignment="Top" FontSize="10"/>
        <ComboBox x:Name="cmbOperatingSystem" HorizontalAlignment="Left" Margin="195,274,0,0" VerticalAlignment="Top" Width="312" Grid.ColumnSpan="2"/>
        <Label x:Name="lblOperatingSystem" Content="Operating System:" HorizontalAlignment="Left" Margin="10,269,0,0" VerticalAlignment="Top" Width="164" Height="27" FontSize="11"/>
        <Border BorderBrush="Black" BorderThickness="1" HorizontalAlignment="Left" Height="1" Margin="10,310,0,0" VerticalAlignment="Top" Width="499" Grid.ColumnSpan="2"/>
        <TextBox x:Name="txtPrimaryUsers" HorizontalAlignment="Left" Height="23" Margin="195,324,0,0" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="312" FontSize="11" Grid.ColumnSpan="2"/>
        <TextBox x:Name="txtComputerDescription" HorizontalAlignment="Left" Height="23" Margin="195,354,0,0" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="312" FontSize="11" Grid.ColumnSpan="2"/>
        <Label x:Name="lblPrimaryUsers" Content="Primary User(s): (DOMAIN\User)" HorizontalAlignment="Left" Margin="10,320,0,0" VerticalAlignment="Top" Width="180" Height="27" FontSize="11"/>
        <Label x:Name="lblComputerDescription" Content="Computer Description:" HorizontalAlignment="Left" Margin="10,350,0,0" VerticalAlignment="Top" Width="180" Height="27" FontSize="11"/>
        <Button x:Name="btnQuit" Content="Quit" HorizontalAlignment="Left" Margin="430,384,0,0" VerticalAlignment="Top" Width="75"/>
        <Button x:Name="btnContinue" Content="Continue" HorizontalAlignment="Left" Margin="350,384,0,0" VerticalAlignment="Top" Width="75"/>
    </Grid>
</Window>
"@

<############################################################################################################################
Functions
############################################################################################################################>

Function Register-Event {
# Event and log handler for entire script, has to be virtually at the top of this script file as used throughout script
# Writes all WARNING and CRITICAL events to console/ log file (name/ path as defined under global script settings)
# If global setting $debug set to $True then writes all INFO, WARNING and CRITICAL to console and log file (name/ path as defined under global script settings)
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$False)]
        [ValidateSet("DEBUG","INFO","WARNING","CRITICAL")]
        [String]
        $level = "INFO",
        [Parameter(Mandatory=$True)]
        [ValidateNotNull()]
        [string]
        $logmessage
    )
    $timeStamp = (Get-Date).toString("yyyy/MM/dd HH:mm:ss")
    $Line = "[$timeStamp] [$level] $logmessage"
    If ($arrConfig) {$logfile = [System.Environment]::ExpandEnvironmentVariables($arrConfig.logfile)}
    If ($level -eq "DEBUG" -and $arrConfig.debug -eq $True) {
        If ($arrConfig.logfile) {Add-Content $logfile  -Value $Line}
        Write-Output $Line | Out-Default 
    }
    ElseIf ($level -eq "WARNING") {
        [System.Windows.MessageBox]::Show(($logmessage),"Warning","OK","Warning") | out-null
        If ($arrConfig.logfile) {Add-Content $logfile  -Value $Line}
        Write-Output $Line | Out-Default 
    }
    ElseIf ($level -eq "CRITICAL") {
        [System.Windows.MessageBox]::Show(($logmessage),"Error","OK","Error") | out-null
        If ($arrConfig.logfile) {Add-Content $logfile  -Value $Line}
        Write-Output $Line | Out-Default
        Disable-UserInterface
    }
    ElseIf ($level -eq "INFO" -and $arrConfig.debug -eq $True) {
        If ($arrConfig.logfile) {Add-Content $logfile  -Value $Line}
        Write-Output $Line | Out-Default 
    }
}

Function Get-Config {
# Parses supplied Config XML file for environment-specific configuration required for this script to execute
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $xmlfile
    )
    Try {
        $fileExists = Test-Path $xmlfile
        If ($fileExists -eq $True) {
            $file = resolve-path($xmlfile)
            [xml]$xml = Get-Content $file
            New-Object -TypeName PsObject -Property @{
                "debug" = $xml.Settings.debug
                "logfile" = $xml.Settings.logfile
                "locationsXML" = $xml.Settings.locationsXML
                "tasksequenceXML" =$xml.Settings.tasksequenceXML
                "sameArchRefresh" = $xml.Settings.sameArchRefresh
                "sameOURefresh" = $xml.Settings.sameOURefresh
                "generateComputerName" = $xml.Settings.generateComputerName
                "ADwebserviceURL" = $xml.Settings.ADwebserviceURL
                "ADwebserviceKey" = $xml.Settings.ADwebserviceKey
                "ExportOULevel" = $xml.Settings.ExportOULevel
                "TSTtsName" = $xml.Settings.TSTtsName
                "TSTisRefresh" = $xml.Settings.TSTisRefresh
                "TSTspoofPlatform" = $xml.Settings.TSTspoofPlatform
                "TSTcomputerName" = $xml.Settings.TSTcomputerName
                "TSTcomputerDomain" = $xml.Settings.TSTcomputerDomain
                "TSTplatformManufacturer" = $xml.Settings.TSTplatformManufacturer
                "TSTplatformModel" = $xml.Settings.TSTplatformModel
                "TSTplatformBIOS" = $xml.Settings.TSTplatformBIOS
                "TSTplatformAssetTag" = $xml.Settings.TSTplatformAssetTag
                "TSTplatformUEFIstate" = $xml.Settings.TSTplatformUEFIstate
            }
        }
        Else {Register-Event "CRITICAL" "Unable to find Config file $xmlfile, unable to proceed"}
    }
    Catch  {Register-Event "CRITICAL" "Unable to obtain script settings/ configuration from $xmlfile"}
}
       
Function Get-FormElement {
# Shows any element in XAML that is programmable
    [CmdletBinding()]
    Param()
    get-variable WPF*
}

Function Get-OSArch {
# Returns Operating System architecture (32-bit or 64-bit)
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param ()
    $osarch = (Get-CIMInstance Win32_OperatingSystem).OSArchitecture
    New-Object -TypeName PsObject -Property @{"OSArchitecture" = $osarch}
    Register-Event "INFO" "Identified system architecture: $osarch"
}

Function Get-PlatformInfo {
# Returns device Manufacturer, Model and BIOS version, populating global variables for use in other functions/ validation
# Note that platformType is appended to psobject by Get-PlatformValid - type is manually defined by user to ensure accuracy
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param()
    try{
        $computerName = [system.environment]::MachineName
        $domainName = (Get-CIMInstance CIM_ComputerSystem).Domain
        $bios = (Get-CIMInstance Win32_BIOS).SMBIOSBIOSVersion
        $manufacturer = (Get-CIMInstance Win32_ComputerSystem).Manufacturer
        $model = (Get-CIMInstance Win32_ComputerSystem).Model
        $assetTag = (Get-CIMInstance Win32_SystemEnclosure).SMBiosAssetTag
        New-Object -TypeName PsObject -Property @{
            "computerName" = $computerName # ignored during Bare-Metal scenario
            "computerDomain" = $domainName # ignored during Bare-Metal scenario
            "platformBIOS" = $bios
            "platformManufacturer" = $manufacturer
            "platformModel" = $model
            "AssetTag" = $assetTag
            }
        Register-Event "INFO" "Identified ComputerName: $computerName DomainName: $domainName"
        Register-Event "INFO" "Retrieved Platform Manufacturer: $manufacturer"
        Register-Event "INFO" "Retrieved Platform Model: $model"
        Register-Event "INFO" "Retrieved Platform BIOS: $bios" 
        Register-Event "INFO" "Retrieved platform AssetTag: $assetTag"
    }
    catch{Register-Event "CRITICAL" "Failed to get information from Win32_Computersystem/ Win32_BIOS"}
}

Function Get-PowerStatus {
# Returns AC power status for use in other functions/ validation - should only be used on devices with battery enclosure, will fail on desktop/ workstation hardware
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [ValidateNotNull()]
        [String]
        $platformType
    )
    If ($platformType -eq "IsLaptop" -or $platformType -eq "IsTablet" -and $mode -ne "Test"){
        If ((Get-CIMInstance Win32_Battery).BatteryStatus -eq 2){
            New-Object -TypeName PsObject -Property @{"pluggedIn" = $True}
            Register-Event "INFO" "Detected Mains Power"
        }
        ElseIf ((Get-CIMInstance Win32_Battery).BatteryStatus -eq 1){
            New-Object -TypeName PsObject -Property @{"pluggedIn" = $False}
            Register-Event "WARNING" "Failed to detect Mains Power, please plug-in the device to enable deployment to continue" 
        }
    }
    Else {
        New-Object -TypeName PsObject -Property @{"pluggedIn" = $True}
        Register-Event "INFO" "Platform is not Laptop/ Tablet or running in 'Test' mode - bypassing AC power status check"
    }
}        

Function Test-ComputerNameSyntax {
# Checks user-supplied/ auto-generated computer hostname conforms to relevant RFC for Windows platform, populates global variable for use elsewhere. 
# Strong validation as this is used to create AD object.
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [ValidateNotNull()]
        [String]
        $name
    )
    If ($name.length -gt 0 -And $name.length -le 15) {
        # Check for illegal characters
        $regex = "[!#""""~`|£ $%^&*()+\.{}@;:<>?\[\]]"
        $validName = $name -notmatch $regex
        # Check for non-English Characters
        If ($validName -eq $True) {
            $regex =  "[^\u0000-\u007F]"
            $validName = $name -notmatch $regex
        }
        New-Object -TypeName PsObject -Property @{"valid" = $validName}
        If ($validName -eq $True) {
            Register-Event "INFO" "ComputerName validation returned : $validName"
        }
        Else {
            Register-Event "WARNING" "Failed to validate ComputerName, invalid characters detected: $name" 
        }
    }
    Else {
        New-Object -TypeName PsObject -Property @{"valid" = $False}
        Register-Event "WARNING" "Failed to validate ComputerName, supplied name too long: $name" 
    }
}

Function Test-UsernameSyntax {
# Validates format of enetered name i.e. DOMAIN\username and that both elements contain characters
# Weak validation as supplied username is then used via user-lookup function
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [ValidateNotNull()]
        [String]
        $name
    )
    $arrUsername = $name.Split("\")
    Register-Event "INFO" "Syntax checking username: $name"
    If ($arrUsername.count -eq 2 -and $arrUsername[1].length -gt 0) {
        New-Object -TypeName PsObject -Property @{"valid" = $True}
        Register-Event "INFO" "Username validation, identified Domain: $($arrUsername[0]) Username : $($arrUsername[1])"
    }
    Else {
        New-Object -TypeName PsObject -Property @{"valid" = $False}
        Register-Event "INFO" "Username syntax validation returned: $isValidUserName"
    }
}

Function Get-ClientGateway {
# Uses WMI to return IPv4-enabled network adapter gateway address for use in location identification
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param()
    $arrGateways = (Get-CIMInstance Win32_networkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway
    foreach ($gateway in $arrGateways) {If ($gateway -ne "") {$clientGateway = $gateway}}
    If ($clientGateway) {
        New-Object -TypeName PsObject -Property @{"IPv4address" = $clientGateway}
        Register-Event "INFO" "Detected client IP gateway: $clientGateway"
    }
    Else {Register-Event "CRITICAL" "Unable to detect Client IPv4 Gateway Address, check IPv4 network adapter/ DHCP configuration"}
}

Function Get-PlatformValid {
# Compares device Manufacturer, Model, BIOS version and UEFI status against pre-defined / tested scenarios listed in XML file
# If device matches compatible scenario OS deployment/ Task Sequence can continue
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $xmlfile,
        [parameter(Mandatory=$true)]
        [String]
        $platformManufacturer,
        [parameter(Mandatory=$true)]
        [String]
        $platformModel,
        [parameter(Mandatory=$true)]
        [String]
        $platformBIOS,
        [parameter(Mandatory=$true)]
        [String]
        $uefiState        
    ) 
    Try {
        $file = resolve-path($xmlfile)
        [xml]$xml = Get-Content $file
        $platformModel = $platformModel.Trim() 
        $platformManufacturer = $platformManufacturer.Trim()
        # Create an array of devices that match detected platform manufacturer
        Register-Event "INFO" "Performing platform validation against: $xmlfile"
        If ($platformManufacturer -like "Dell*"){
			Register-Event "INFO" "Dell device detected, overriding CMI-obtained manufacturer value to: 'Dell Inc.'"
			$arrDevices = $xml.ArrayOfDeploymentRules.DeploymentRules.Vendor | Where-Object {$_.name -eq "Dell Inc."} | Select-Object -Expand Platform 
		}
		Else {
            $arrDevices = $xml.ArrayOfDeploymentRules.DeploymentRules.Vendor | Where-Object {$_.name -eq $platformManufacturer} | Select-Object -Expand Platform
        }
        If ($arrDevices) {
            $arrDevices | ForEach-Object {
                # Check there is a model defined that matches detected platform
                If ($_.model -eq $platformModel) {
                    $supportedModel = $True
                    $minSupportedBIOS = $_.MinSupportedBios
                    $UEFISupport = $_.UEFISupport
                    $type = $_.type
                }
            }
            # If matching manufacturer/ model found review BIOS/ UEFI configuration
            If ($supportedModel -eq $True) {
                # Check BIOS version meets requirements
                If ($platformBIOS -ge $minSupportedBIOS -or $minSupportedBIOS -like "ANY") {
                    $biosValidated = $True
                    Register-Event "INFO" "Platform BIOS validation passed, version detected: $platformBIOS"
                }
                Else {
                    $biosValidated = $False
                    Register-Event "CRITICAL" "Incorrect/ unsupported BIOS version for this model, please update to at least : $minSupportedBIOS"            
                }
                # Check UEFI configuration meets requirements
                If ($UEFISupport -like "UEFI" -and $uefiState -like "TRUE") {
                    Register-Event "INFO" "Platform UEFI state validation passed"
                    $uefiValidated = $True
                }
                ElseIf ($UEFISupport -like "Legacy" -and $uefiState -like "FALSE"){
                    Register-Event "INFO" "Platform UEFI state validation passed"
                    $uefiValidated = $True
                }
                ElseIf ($UEFISupport -like "ANY") {
                    Register-Event "INFO" "Platform UEFI state validation passed"
                    $uefiValidated = $True
                }
                Else {
                    $uefiValidated = $False
                    Register-Event "CRITICAL" "Incorrect UEFI configuration detected. Platform firmware should be configured for: $UEFISupport"            
                    }
                # Output result of validation/ return platform type
                If ($biosValidated -eq $True -and $uefiValidated -eq $True) {
                    # Validation was successful update $arrPlatform with boolean validPlatform property
                    Register-Event "INFO" "Platform validation succeeded"
                    Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "validPlatform" -Value $True
                    # Update $arrPlatform with user-defined platform type
                    Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "platformType" -Value $type
                    Register-Event "INFO" "Platform type, as defined in XML: $($type)"
                }
                Else {
                    Register-Event "CRITICAL" "Platform validation failed, unable to proceed with Task Sequence"
                    Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "validPlatform" -Value $False                    
                }
            }
            Else {
                Register-Event "CRITICAL" "Untested / undefined model. You need to update $xmlfile with platform information for Manufacturer: $platformManufacturer and Model: $platformModel"
                Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "validPlatform" -Value $False
            }
        }	
        Else {
            Register-Event "CRITICAL" "Untested / undefined manufacturer. You need to update $xmlfile with platform information for Manufacturer: $platformManufacturer and Model: $platformModel"
            Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "validPlatform" -Value $False
        }
	}
    Catch {
        Register-Event "CRITICAL" "Check Platform Valid: Error opening $xmlfile file, check file is in same folder as ps1 script" 
    }
}

Function Disable-UserInterface {
# Disables UI controls in the event of a critical failure - precents wiping disks/ data in the event of validation failing/ required service being unavailable
    [CmdletBinding()]
    Param()
    If ($arrConfig.XAMLparsed -eq $True) {
        $WPFcmbTargetLocation.IsEnabled = $false
        $WPFcmbTargetOU.IsEnabled = $false
        $WPFcmbOperatingSystem.IsEnabled = $false
        $WPFtxtComputerName.IsEnabled = $false
        $WPFtxtPrimaryUsers.IsEnabled = $false
        $WPFtxtTargetDomain.IsEnabled = $false
        $WPFrdoAccountResetNo.IsEnabled = $false
        $WPFrdoAccountResetYes.IsChecked = $true
        $WPFrdoAccountResetYes.IsEnabled = $false
        $WPFrdoAccountResetYes.IsEnabled = $false
        $WPFchkMoveComputer.IsEnabled = $false
        $WPFtxtComputerDescription.IsEnabled = $false
        $WPFtxtComputerName.Text = "Error"
        $WPFtxtTargetDomain.Text = "Error"
        $WPFbtnContinue.IsEnabled = $false
    }
}

Function Get-OrganisationalUnits {
# Parses WebService exported list of OUs within supplied XML file, then populates combobox, indenting child OUs to ease identification/ navigation
# Automatically selects existing OU for Computer Account during Refresh Scenario/ "default" OU for Location during Bare Metal Scenario
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $xmlfile,
        [parameter(Mandatory=$true)]
        [String]
        $strPath,
        [parameter(Mandatory=$true)]
        [int]
        $intLevel
    )
	Try {
		$file = resolve-path($xmlfile)
		[xml]$xml = Get-Content $file
		$ns = @{x="http://maikkoster.com/Deployment"}
		If ($intLevel -eq 0) {
			[int]$intLevel = 1
			$xpath = "/x:ArrayOfOU/x:OU"
			Select-Xml -Path $file -Xpath $xpath -Namespace $ns | Select-Object -ExpandProperty Node | Sort-Object -Property Name | ForEach-Object {
				$modifiedpath = ($_.path).replace("$($arrLocationSettings.JoinDomain)/","") # protects against using ADEX.asmx WebService URL to get OUs
                $WPFcmbTargetOU.Items.Add([pscustomobject]@{ Name = $_.Name; Value = $modifiedpath }) | out-null
                If ($arrPlatform.isRefresh -eq $True -and $arrConfig.sameOURefresh -eq $True) {
                    If ($modifiedpath -eq $arrPlatform.existingADComputerOU) {
                        $WPFcmbTargetOU.SelectedValue = $modifiedpath
                        Register-Event "INFO" "Overriding LocationDefaultOU as Config flag 'sameOURefresh' set to 'True'"                        
                    }
                }
				ElseIf ($modifiedpath -eq $arrLocationSettings.LocationDefaultOU) {$WPFcmbTargetOU.SelectedValue = $modifiedpath}
				$xpath = "/x:ArrayOfOU/x:OU[x:Path='$($_.path)']/x:ChildOUs/x:OU"
                Get-OrganisationalUnits $xmlfile $xpath $intLevel
			}
			$WPFcmbTargetOU.DisplayMemberPath ='Name'
			$WPFcmbTargetOU.SelectedValuePath ='Value'		
		}
		ElseIf ($intLevel -ge 1) {
			$spacer = "  "* $intLevel
			$intLevel = $intLevel +1	
			Select-Xml -Path $file -Xpath $strPath -Namespace $ns | Select-Object -ExpandProperty Node | Sort-Object -Property Name | ForEach-Object {
				$modifiedpath = ($_.path).replace("$($arrLocationSettings.JoinDomain)/","") # protects against using ADEX.asmx WebService URL to get OUs
                $WPFcmbTargetOU.Items.Add([pscustomobject]@{ Name = "$spacer $($_.name)"; Value = $modifiedpath  }) | out-null
				If ($arrPlatform.isRefresh -eq $True -and $arrConfig.sameOURefresh -eq $True) {
                    If ($modifiedpath -eq $arrPlatform.existingADComputerOU) {
                        $WPFcmbTargetOU.SelectedValue = $modifiedpath
                        Register-Event "INFO" "Overriding LocationDefaultOU as Config flag 'sameOURefresh' set to 'True'"
                    }
                }
				ElseIf ($modifiedpath -eq $arrLocationSettings.LocationDefaultOU) {$WPFcmbTargetOU.SelectedValue = $modifiedpath }
                $newpath = "$strPath[x:Path='$($_.path)']/x:ChildOUs/x:OU"
                Get-OrganisationalUnits $xmlfile $newpath $intLevel
			}		
		}
	}
	Catch {
		Register-Event "CRITICAL" "Error processing OU's in: $xmlfile" 
	}
}

Function Enable-RefreshScenarioUI {
# Restricts UI controls to those only needed during Refresh Scenario, i.e. wipe and reload whilst retain user data/ UDA/ hostname etc.
# These options can be changed, but consider template T/S will use USMT to backup and restore user data, if reprovisioning go for Bare Metal via WinPE, it will be faster
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $computerDomain,
        [parameter(Mandatory=$true)]
        [String]
        $computerName
    )
    $WPFcmbTargetLocation.IsEnabled = $true
    $WPFcmbTargetOU.IsEnabled = $true
    $WPFcmbOperatingSystem.IsEnabled = $true
    $WPFtxtComputerName.IsEnabled = $false
    $WPFtxtPrimaryUsers.IsEnabled = $true
    $WPFtxtTargetDomain.IsEnabled = $false
    $WPFrdoAccountResetNo.IsEnabled = $false
    $WPFrdoAccountResetYes.IsChecked = $true
    $WPFrdoAccountResetYes.IsEnabled = $false
    $WPFrdoAccountResetYes.IsEnabled = $false
    $WPFchkMoveComputer.IsEnabled = $true
    $WPFtxtComputerDescription.IsEnabled = $true
    $WPFtxtComputerName.Text = $computerName
    $WPFtxtTargetDomain.Text = $computerDomain
    Register-Event "INFO" "Refresh Task Sequence UI/Interface Options Set"
}


Function Get-LocationSettings {
# Pulls per-location settings from user-defined Locations.xml file, based on supplied location ID
# Loctaion settings populate global variables/ are used within the UI
# If you want to create location-specific settings you need to edit this Funciton and Set-TSVars function
    [CmdletBinding()]
	[OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $id,
        [parameter(Mandatory=$true)]
        [String]
        $xmlfile
    )
    Try {
        $file = resolve-path($xmlfile)
        [xml]$xml = Get-Content $file
        $arrSettings = $xml.ArrayOfLocationSettings.LocationSettings.Location  | Where-Object {$_.id -eq $id} | Select-Object -Expand Settings
        If ($arrSettings) {
			New-Object -TypeName PsObject -Property @{
                "LocationID" = $id
				"InputLocale" = $arrSettings.InputLocale
                "UserLocale" = $arrSettings.UserLocale
                "UILanguage" = $arrSettings.UILanguage
				"SecondaryUILanguage" = $arrSettings.SecondaryUILanguage
				"TimeZoneName" = $arrSettings.TimeZoneName
				"JoinDomain" = $arrSettings.JoinDomain
				"LocationDefaultOU" = $arrSettings.LocationDefaultOU
				"SitePrefix" = $arrSettings.SitePrefix
				"LaptopPrefix" = $arrSettings.LaptopPrefix
				"WorkstationPrefix" = $arrSettings.WorkstationPrefix
                "TabletPrefix" = $arrSettings.TabletPrefix
			}
		    $WPFtxtTargetDomain.Text = $arrSettings.JoinDomain
            $WPFtxtTargetDomain.IsEnabled = $false	
	        Register-Event "INFO" "================= START Location Settings ================="
            Register-Event "INFO" "Obtained Location Settings for LoctionID: $id from $xmlfile"
            Register-Event "INFO" "InputLocale: $($arrSettings.InputLocale)"
            Register-Event "INFO" "UserLocale: $($arrSettings.UserLocale)"
            Register-Event "INFO" "UILanguage: $($arrSettings.UILanguage)"
            Register-Event "INFO" "SecondaryUILanguage: $($arrSettings.SecondaryUILanguage)"
            Register-Event "INFO" "TimeZoneName: $($arrSettings.TimeZoneName)"
            Register-Event "INFO" "JoinDomain: $($arrSettings.JoinDomain)"
            Register-Event "INFO" "LocationDefaultOU: $($arrSettings.LocationDefaultOU)"
            Register-Event "INFO" "SitePrefix: $($arrSettings.SitePrefix)"
			Register-Event "INFO" "================= END Location Settings ================="		
		}
		Else {Register-Event "CRITICAL" "Get-LocationSettings: Error getting loctaion settings from: $xmlfile" }
    }
    Catch {
        Register-Event "CRITICAL" "Get-LocationSettings: Error opening $xmlfile file, check file is in same folder as ps1 script" 
    }
}

Function Get-AllLocationIDName {
# Pulls location IDs and Names from Locations.xml file and populates global variable with array
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $xmlfile
    )
    Try {
        $file = resolve-path($xmlfile)
        [xml]$xml = Get-Content $file
        $locations = $xml.ArrayOfLocationSettings.LocationSettings.Location | Select-Object id,name
        $locations
        Register-Event "INFO" "Obtained LocationIDs from $xmlfile"
    }
    Catch {
        Register-Event "CRITICAL" "Get-AllLocationIDName: Error opening $xmlfile file, check file is in same folder as ps1 script" 
    }
}

Function Add-Location {
# Uses $global:arrLocationIDName to add locations to combobox WPFcmbTargetLocation
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $clientLocationName
    )
	foreach ($location in $arrLocationIDName) {
		$WPFcmbTargetLocation.Items.Add([pscustomobject]@{ Name = $location.name; Value = $location.id }) | out-null
		If ($location.name -eq $clientLocationName) {$WPFcmbTargetLocation.SelectedValue = $location.id}
        Register-Event "INFO" "Added location: $($location.name) locationID: $($location.id) to location select combobox"
    }
    $WPFcmbTargetLocation.DisplayMemberPath ='Name'
	$WPFcmbTargetLocation.SelectedValuePath ='Value'
}

Function Get-AvailableOperatingSystem {
# Pulls available Operating Systems from TaskSequences.xml, specifically "osName" and "osString" attributes and populates a combobox
# This enables multi-OS (32-bit/ 64-bit image), or different SKU deployment from the same Task Sequence
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $xmlfile,
        [parameter(Mandatory=$true)]
        [String]
        $tsname
    )    
    Try {
        $file = resolve-path($xmlfile)
        [xml]$xml = Get-Content $file
        $arrOS = $xml.TaskSeqeunces.TaskSequence  | Where-Object {$_.Name -eq $tsname} 
        If ($arrOS) {
            # Check requirement to validate detected hardware platform for this Task Sequence, if True, capture XML file name with deployment rules
            If ($arrOS.chkPlatformValid -eq "TRUE") {
                Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "chkPlatformValid" -Value $True
                Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "chkPlatformXML" -Value $arrOS.chkPlatformXML
            }
            Else {
                Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "chkPlatformValid" -Value $False
            }

            $osarch = Get-OSArch
            $arrOS | Select-Object -Expand Option | ForEach-Object {
                $WPFcmbOperatingSystem.Items.Add([pscustomobject]@{ Name = $_.osName; Value = $_.osString }) | out-null
                # Refresh only, review current architecture and default image for Task Sequence against Config flag sameArchRefresh
                If ($arrPlatform.isRefresh -eq $True) {
                    # If sameArchRefresh set to 'True' ensure a 64-bit image is selected by default, irrespective of Default for Task Sequence
                    If ($_.isX64 -eq "TRUE" -and $osarch.OSArchitecture -like "64*" -and !$selectedDefaultOS -and $arrConfig.sameArchRefresh -eq $True) {
                        Register-Event "INFO" "Default Operating System Image for Task Sequence overridden due to Config flag 'sameArchRefresh' set to 'True'"
                        $WPFcmbOperatingSystem.SelectedValue = $_.osString
                        $selectedDefaultOS = $True
                    }
                    # If sameArchRefresh set to 'True' ensure a 32-bit image is selected by default, irrespective of Default for Task Sequence
                    ElseIf ($_.isX64 -eq "FALSE" -and $osarch.OSArchitecture -like "32*" -and !$selectedDefaultOS -and $arrConfig.sameArchRefresh -eq $True) {
                        Register-Event "INFO" "Default Operating System Image for Task Sequence overridden due to Config flag 'sameArchRefresh' set to 'True'"
                        $WPFcmbOperatingSystem.SelectedValue = $_.osString
                        $selectedDefaultOS = $True
                    }
                    # Otherwise, just set the default image for Task Sequence
                    ElseIf ($_.default -eq "TRUE" -and !$selectedDefaultOS) {
                        $WPFcmbOperatingSystem.SelectedValue = $_.osString
                        $selectedDefaultOS = $True
                    }
                }
                # For bare-metal scenarios, just select default image for Task Sequence
                Else {
                    If ($_.default -eq "TRUE") {
                        $WPFcmbOperatingSystem.SelectedValue = $_.osString
                        $selectedDefaultOS = $True
                    }
                }
                Register-Event "INFO" "Added Operating System: $($_.osString) to O/S selection combobox"
            }
                $WPFcmbOperatingSystem.DisplayMemberPath ='Name'
                $WPFcmbOperatingSystem.SelectedValuePath ='Value'
        }
        Else {Register-Event "CRITICAL" "Untested/ invalid Task Sequence for HTA use"}
    }
    Catch {
        Register-Event "CRITICAL" "Get-AvailableOperatingSystem: Error opening $file file, check file is in same folder as ps1 script" 
    }
}
Function Get-ClientLocationIDName {
# Uses detected IPv4 gateway address to find client's existing LocationID against locations defined in Locations.xml file
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $gw,
        [parameter(Mandatory=$true)]
        [String]
        $xmlfile
    )
    Try {
        $file = resolve-path($xmlfile)
        [xml]$xml = Get-Content $file	
		$clientLocation = $xml.ArrayOfLocationSettings.LocationSettings.Location | Where-Object {$_.Settings.DefaultGateway -eq $gw} | Select-Object id, name
		If ($clientLocation) {          
             New-Object -TypeName PsObject -Property @{
                 "ClientLocationID" = $clientLocation.id
                 "ClientLocationName" = $clientLocation.name
                }
			Register-Event "INFO" "Client LocationID : $($clientLocation.id), Client Location Name: $($clientLocation.name)"
        }
        Else {Register-Event "CRITICAL" "Unable to match client IPv4 gateway: $gw against location in $xmlfile"}
    }
    Catch {
        Register-Event "CRITICAL" "Get-ClientLocationIDName: Error opening $xmlfile file, check file is in same folder as ps1 script" 
    }           
}

Function New-ComputerName {
# Generates computer hostname based upon AssetTag and information supplied in Locations.xml
# This function does not run for Virtual Machines or if location "SitePrefix" is set to "N/A"
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $assettag,
        [parameter(Mandatory=$true)]
        [String]
        $platformType
    )
    If ($arrConfig.generateComputerName -eq $True -and $assettag -and $platformType -ne "isVM" -and $arrLocationSettings.SitePrefix -ne "N/A") {
        If ($platformType -eq "isLaptop") {$generatedName = "$($arrLocationSettings.SitePrefix)$($arrLocationSettings.LaptopPrefix)$($assettag)"}
        ElseIf ($platformType -eq "isDesktop") {$generatedName = "$($arrLocationSettings.SitePrefix)$($arrLocationSettings.WorkstationPrefix)$($assettag)"}
        ElseIf ($platformType -eq "isTablet") {$generatedName = "$($arrLocationSettings.SitePrefix)$($arrLocationSettings.TabletPrefix)$($assettag)"}
        $WPFtxtComputerName.Text = $generatedName
        Register-Event "INFO" "Computer Name generated as : $generatedName"
    }
}

Function Select-Location {
# Executes when user selects a new loctaion from combobox - repopulates UI information/ global variables with settings for the new location
    [CmdletBinding()]
    Param()
    Register-Event "INFO" "Selected Location ID: $($WPFcmbTargetLocation.SelectedValue)"
    $global:arrLocationSettings = Get-LocationSettings $WPFcmbTargetLocation.SelectedValue $arrConfig.locationsXML
    # Flush OU comboBox, this may be different Domain for new location
    $WPFcmbTargetOU.Items.Clear()
    # Populate OU ComboBox with New Location Information
    $JoinDomainXML = $arrLocationSettings.JoinDomain
    $JoinDomainXML = "$($JoinDomainXML.replace(".","-"))-OUs.xml"
    Get-OrganisationalUnits $JoinDomainXML "." 0
    New-ComputerName $arrPlatform.AssetTag $arrPlatform.platformType
}

Function Set-TSVARS {
# Based on Locations settings and user-interaction sets Task Sequence variables used within the Task Sequence logic
    [CmdletBinding()]
    Param()
    # In 'Test' mode SMS COM Object not available to set Task Sequence Variables
    If ($mode -notlike "Test") {
        $tsenv.value("OSDQuitTS") = "FALSE"
        $tsenv.value("OSDComputerName") = ($WPFtxtComputerName.Text).ToUpper()
        $tsenv.value("OSDMachineDomain") = $arrLocationSettings.JoinDomain
        $tsenv.value("OSDInputLocale") = $arrLocationSettings.InputLocale
        $tsenv.value("OSDUserLocale") = $arrLocationSettings.UserLocale
        $tsenv.value("OSDTimeZone") = $arrLocationSettings.TimeZoneName
        $tsenv.value("OSDPlatformType") = $arrPlatform.platformType
        $tsenv.value("OSDLocationID") = $arrLocationSettings.LocationID
        $tsenv.value("OSDLocationName") = $WPFcmbTargetLocation.Text
        $tsenv.value("OSDMachineObjectOU") = $WPFcmbTargetOU.SelectedValue
        $tsenv.value("OSDUILanguage") = $arrLocationSettings.UILanguage
        If ($arrLocationSettings.SecondaryUILanguage) {$tsenv.value("OSDSecondaryUILanguage") = $arrLocationSettings.SecondaryUILanguage}
        If ($WPFtxtComputerDescription.Text -gt "") {$tsenv.value("machineDescription") =$WPFtxtComputerDescription.Text}
        If ($WPFtxtPrimaryUsers.Text -gt "") {$tsenv.value("SMSTSUdaUsers") =$WPFtxtPrimaryUsers.Text}
        $tsenv.value("osEdition") = $WPFcmbOperatingSystem.SelectedValue
    }
    Else {Register-Event "INFO" "Task Sequence variables NOT set as 'Test' mode detected/ COM Object therefore not available"}

    Register-Event "INFO" "================= START TS Vars Applied ================="
    Register-Event "INFO" "OSDQuitTS: FALSE"
    Register-Event "INFO" "OSDComputerName: $(($WPFtxtComputerName.Text).ToUpper())"
    Register-Event "INFO" "OSDMachineDomain: $($arrLocationSettings.JoinDomain)"
    Register-Event "INFO" "OSDInputLocale: $($arrLocationSettings.InputLocale)"
    Register-Event "INFO" "OSDUserLocale: $($arrLocationSettings.UserLocale)"
    Register-Event "INFO" "OSDTimeZone: $($arrLocationSettings.TimeZoneName)"
    Register-Event "INFO" "OSDPlatformType: $($arrPlatform.platformType)"
    Register-Event "INFO" "OSDLocationID: $($arrLocationSettings.LocationID)"
    Register-Event "INFO" "OSDLocationName: $($WPFcmbTargetLocation.Text)"
    Register-Event "INFO" "OSDMachineObjectOU: $($WPFcmbTargetOU.SelectedValue)" 
    Register-Event "INFO" "OSDUILanguage: $($arrLocationSettings.UILanguage)"
    Register-Event "INFO" "OSDSecondaryUILanguage: $($arrLocationSettings.SecondaryUILanguage)"
    If ($WPFtxtComputerDescription.Text -gt ""){Register-Event "INFO" "Computer Description: $($WPFtxtComputerDescription.Text)"}
    If ($WPFtxtPrimaryUsers.Text -gt "") {Register-Event "INFO" "SMSTSUdaUsers: $($WPFtxtPrimaryUsers.Text)"}
    Register-Event "INFO" "osEdition: $($WPFcmbOperatingSystem.SelectedValue)"
    Register-Event "INFO" "================= END TS Vars Applied  ================="
}

Function Connect-SMSTSEnv {
# Tries to establish Microsoft.SMS.TSEnvironment COM Object, only available withihn Task Sequence
# Essential due to need to set Task Sequence variables that persist reboots etc during OSD
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param()
    try{
        $global:tsenv = New-Object -COMObject Microsoft.SMS.TSEnvironment
        New-Object -TypeName PsObject -Property @{"available" = $True}
        Register-Event "INFO" "Connected to SMS TSEnvironment Provider"
    }
    catch {
        New-Object -TypeName PsObject -Property @{"available" = $False}
        Register-Event "CRITICAL" "Failed to connect to SMS TSEnvironment Provider, unable to proceed" 
    }
}

Function Connect-TSProgressUI {
# Tries to establish Microsoft.SMS.TSProgressUI COM Object
# Essential as without this we are unable to hide Task Sequence Progress UI, which will sit on top of the OSZ Wizard interface
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param()
    try{
        $global:tsui = New-Object -COMObject Microsoft.SMS.TSProgressUI
        New-Object -TypeName PsObject -Property @{"available" = $True}
        Register-Event "INFO" "Connected to SMS TSProgressUI Provider"
    }
    catch {
        New-Object -TypeName PsObject -Property @{"available" = $False}
        Register-Event "CRITICAL" "Failed to connect to SMS TSProgressUI Provider" 
    }
}

Function Hide-TSProgressUI {
# Leverages Microsoft.SMS.TSProgressUI COM Object to hide the TS Progress UI at script load
    [CmdletBinding()]
    Param()
    try {
        $tsui.closeprogressdialog()
        Register-Event "INFO" "TSProgressUI Hidden"  
    }
    catch {
        Register-Event "WARNING" "Failed to hide SMS TSProgressUI Provider" 
    }
}

Function Show-TSProgressUI {
# Leverages Microsoft.SMS.TSProgressUI COM Object to show the TS Progress UI at script completion
    [CmdletBinding()]
    Param()
    try {
        $orgName = $tsenv.value("_SMSTSOrgName")
        $pkgName = $tsenv.value("_SMSTSPackageName")
        $customMessage = $tsenv.value("_SMSTSCustomProgressDialogMessage")
        $currentAction = $tsenv.value("_SMSTSCurrentActionName")
        $nextInstructor = $tsenv.value("_SMSTSNextInstructionPointer")
        $maxStep = $tsenv.value("_SMSTSInstructionTableSize")
        $tsui.ShowTSProgress($orgName, $pkgName, $customMessage, $currentAction, $nextInstructor, $maxStep);
        Register-Event "INFO" "TSProgressUI Now Showing"
    }
    catch {
        Register-Event "WARNING" "Failed to show SMS TSProgressUI Provider" 
    }
}

Function Connect-WebService {
# Connects to Maik Koster's Deployment Web Service, as defined in global varibales at the top of this script
# Essential as this web service is used for a variety of funcitons in this script
    [CmdletBinding()]
    Param()
    try {
        $global:ADWebS = New-WebServiceProxy -Uri $arrConfig.ADwebserviceURL -ErrorAction Stop
        New-Object -TypeName PsObject -Property @{"available" = $True}
        Register-Event "INFO" "Connected to WebService : $($arrConfig.ADwebserviceURL)"
    }
    catch [System.Net.WebException] {
        New-Object -TypeName PsObject -Property @{"available" = $False}
        Register-Event "CRITICAL" "Failed to connect to WebService: $($arrConfig.ADwebserviceURL)" 
    }   
}

Function Move-ComputerOU {
# Uses Maik Koster's Deployment Web Service to move Computer Account from existing OU to user-selected OU
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $domain,
        [parameter(Mandatory=$true)]
        [String]
        $hostname,
        [parameter(Mandatory=$true)]
        [String]
        $ou
    )
    try {
        $result = $ADWebS.MoveComputerToOU($domain, $hostname, $ou)
        New-Object -TypeName PsObject -Property @{"result" = $result}
        If ($result -like "True") {
            Register-Event "INFO" "Successfully moved computer object: $hostname to target OU: $ou"
        }
        Else {
            Register-Event "WARNING" "Failed to move computer object: $hostname to target OU: $ou"
        }
    }
    catch {
        Register-Event "CRITICAL" "Failed to move Computer Account: $hostname to target OU: $ou via AD WebService" 
    }
}

Function Get-ComputerAttribute {
# Uses Maik Koster's Deployment Web Service to obtain requested attribute for a given Computer Account
    [CmdletBinding()]
	[OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $domain,
        [parameter(Mandatory=$true)]
        [String]
        $hostname,
        [parameter(Mandatory=$true)]
        [String]
        $attribute
    )
    try {
        Register-Event "INFO" "Using AD WebService to find $attribute for Computer account: $domain\$hostname"   
		$result = $ADWebS.GetComputerAttribute($domain, $hostname, $attribute)
		If ($result) {
			Register-Event "INFO" "WebService returned Computer $attribute : $result"   
			New-Object -TypeName PsObject -Property @{$attribute = $result}
		}
		Else {Register-Event "INFO" "Unable to return Computer $attribute, WebService unable to find Computer account: $domain\$hostname"   }
    }
    catch {
        Register-Event "CRITICAL" "Failed to retrieve Computer $attribute via AD WebService" 
    }
}


Function Set-ComputerDescription {
# Uses Maik Koster's Deployment Web Service to set description for given Computer Account
    [CmdletBinding()]
	[OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $domain,
        [parameter(Mandatory=$true)]
        [String]
        $hostname,
        [parameter(Mandatory=$true)]
        [String]
        $description
    )
    try {
        Register-Event "INFO" "Using AD WebService to set description for Computer account: $domain\$hostname"   
		$result = $ADWebS.SetComputerDescription($domain, $hostname, $description)
		If ($result) {
			Register-Event "INFO" "WebService returned $result"   
			New-Object -TypeName PsObject -Property @{"result" = $result}
		}
		Else {Register-Event "INFO" "Unable to set description for Computer account: $domain\$hostname"   }
    }
    catch {
        Register-Event "WARNING" "Failed to set description for Computer via AD WebService" 
    }
}

Function Get-ComputerExist {
# Uses Maik Koster's Deployment Web Service to check whether computer account with user-defined/ auto-generated hostname exists
    [CmdletBinding()]
	[OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $domain,
        [parameter(Mandatory=$true)]
        [String]
        $hostname
    )
    try {
        $result = ($ADWebS.DoesComputerExist($domain, $hostname)).ToString()
		New-Object -TypeName PsObject -Property @{"DoesComputerExist" = $result}
		Register-Event "INFO" "Get-ComputerExist for $domain\$hostname returned: $result"
	}
	catch {
        Register-Event "CRITICAL" "Failed to check Computer Exists via AD WebService" 
		}
}

Function Get-UserExist {
# Uses Maik Koster's Deployment Web Service to check whether user account(s) entered under "Primary Users" exist
    [CmdletBinding()]
	[OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $username
    )
    try {
        If (($username).Contains(",")) {$arrUsers = $username.split(",")}
		Else {$arrUsers = $username}
		[int]$countUserErrors = 0
		$arrUsers | ForEach-Object {
			$isValidUserName = Test-UsernameSyntax $_
			If ($isValidUserName.valid -eq $True) {
				$arrUsername = $_.split("\")
				$userExists = $ADWebS.DoesUserExist($arrUsername[0], $arrUsername[1])
		        If ($userExists -like "False") {
                    $countUserErrors = $countUserErrors +1
                    Register-Event "WARNING" "Unable to find user: $($arrUsername[0])\$($arrUsername[1])" 
                }
                ElseIf ($userExists -like "True") {
                    Register-Event "INFO" "Found user account: $($arrUsername[1]) in domain: $($arrUsername[0])"
                }
			}				
		}
		If (!$countUserErrors -gt 0) {
			New-Object -TypeName PsObject -Property @{"DoUsersExist" = $True}
			Register-Event "INFO" "Found all supplied User Accounts defined under Primary Users"
		}
		Else {
			New-Object -TypeName PsObject -Property @{"DoUsersExist" = $False}
			Register-Event "WARNING" "Identified errors with one or more supplied Primary Users, please review accounts" 
		}
	}
    catch {
        Register-Event "WARNING" "Failed to check UDA User Exists via AD WebService" 
    }
}
Function Start-Build {
# Trigger post-validation success actions - i.e. all pre-flight checks have passed and the TS can continue as planned
    [CmdletBinding()]
    Param()
    Set-TSVARS
    # Under 'Test' mode do not try to connect to TS Progress UI COM Object
    If ($mode -notlike "Test") {Show-TSProgressUI}
    $Form.Close()
}

Function Get-ValidationToProceed {
# All pre-flight checks that are performed prior to proceeding with the Task Sequence
    [CmdletBinding()]
	[OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $platformType  
    )
    # Universal, basic checks
    # Confirm AC power present, if not allow user to remediate
    $CheckPower = Get-PowerStatus $platformType
    If ($CheckPower.pluggedIn -ne $True) {
        $validation = $False
        Register-Event "INFO" "Power status validation FAILED"
    }
    Else {
        $validation = $True
        Register-Event "INFO" "Power status validation PASSED"
    }
    # Bare-Metal Task Sequence specific checks
    If ($arrPlatform.isRefresh -eq $False -and $validation -eq $True){
        # Check Computer Name
        $IsValidComputerName = Test-ComputerNameSyntax $WPFtxtComputerName.Text
        If ($IsValidComputerName.valid -ne $True) {$validation = $False}
        Else {       
            # Check computer account doesn't already exist
            Register-Event "INFO" "Checking computer account does not exist via AD WebService"
            $ADComputerExists = (Get-ComputerExist ($WPFtxtTargetDomain.text).Trim() ($WPFtxtComputerName.text).Trim()).DoesComputerExist
            # Account exists
            If ($ADComputerExists -like "TRUE") {
                # Get existing computer DN, from there work out existing OU Path
                $existingADComputerDN = (Get-ComputerAttribute ($WPFtxtTargetDomain.text).Trim() ($WPFtxtComputerName.text).Trim() "distinguishedName").distinguishedName
                If ($existingADComputerDN) {$existingADComputerOU = $existingADComputerDN.Replace("CN=$(($WPFtxtComputerName.text).Trim()),", "")}
                Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "existingADComputerOU" -Value $existingADComputerOU
                # Check whether manual overide/ reset radio box is checked, if so warn user they HAVE to reset account
                If ($WPFrdoAccountResetYes.IsChecked) {
                    $validation = $True
                    Register-Event "INFO" "Computer account validation PASSED with warnings, manual computer account reset required"
                    Register-Event "WARNING" "Please remember to manually reset computer account in AD Users and Computers"
                }
                # If manual overide/ reset radio box not checked but account already exists warn user 
                ElseIf ($WPFrdoAccountResetYes.IsChecked -eq $False) {
                    $validation = $False
                    Register-Event "WARNING" "Computer account already exists. Reset the account in AD Users and Computer then set 'Existing Account Reset' to 'Yes'"
                    Register-Event "INFO" "Computer Account validation FAILED, account already exists and 'Existing Account Reset' set to 'No'"
                }
            }
            # Account doesn't exist, no risk of duplication/ domain join failing
            ElseIf ($ADComputerExists -like "FALSE") {
                $validation = $True
                Register-Event "INFO" "Computer Account Validation PASSED"
            }
            # Account lookup returned something else, default to error state
            Else {
                $validation = $False
                Register-Event "INFO" "Computer Account Validation FAILED"
            }
        }
    }
    # Refresh Task Sequence specific checks
    ElseIf ($arrPlatform.isRefresh -eq $True -and $validation -eq $True){
        $IsValidComputerName = Test-ComputerNameSyntax $WPFtxtComputerName.Text
        If ($IsValidComputerName.valid -ne $True) {$validation = $False}
        Else {    
            # Find existing Computer Account information, based upon text box entried - enabling user to change hostname
            $ADComputerExists = (Get-ComputerExist ($WPFtxtTargetDomain.text).Trim() ($WPFtxtComputerName.text).Trim()).DoesComputerExist
            $existingADComputerDN = (Get-ComputerAttribute ($WPFtxtTargetDomain.text).Trim() ($WPFtxtComputerName.text).Trim() "distinguishedName").distinguishedName
            If ($existingADComputerDN) {$existingADComputerOU = $existingADComputerDN.Replace("CN=$(($WPFtxtComputerName.text).Trim()),", "")}
            Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "existingADComputerOU" -Value $existingADComputerOU -Force
        }
    }

    # Universal, complex checks
    # Check Primary Users/ UDA names are valid/ or blank, fail but allow change
    If ($validation -eq $True -and $WPFtxtPrimaryUsers.text -ne "") {
        Register-Event "INFO" "Primary User Account(s) defined, checking they exist"
        $ADUserExists = (Get-UserExist $WPFtxtPrimaryUsers.text).DoUsersExist
    }
    ElseIf ($validation -eq $True -and $WPFtxtPrimaryUsers.text -eq "") {
        $ADUserExists = $True #False TRUE, no accounts to verify existence of
        Register-Event "INFO" "NO Primary User Account(s) defined, by-passing checks"
    }
    # User(s) doesn't exist/ error finding valid user in supplied domain
    If ($validation -eq $True -and $ADUserExists -eq $False) {
        $validation = $False
        Register-Event "INFO" "Primary User Account Validation FAILED"
    }
    # Supplied user(s) exists
    ElseIf ($validation -eq $True -and $ADUserExists -eq $True) {
        $validation = $True
        Register-Event "INFO" "Primary User Account Validation PASSED"
    }

    # Check whether selected OU is different to existing account OU
    If ($validation -eq $True -and $ADComputerExists -like "TRUE" -and $WPFcmbTargetOU.SelectedValue -ne $arrPlatform.existingADComputerOU) {
        Register-Event "INFO" "Target OU for Computer Account does not match existing OU"
        # CheckBox to move Computer Account is enabled, try to move COmputer Account to selected OU
        If ($WPFchkMoveComputer.IsChecked) {
            # Do not want to move computer accuunt OU in 'Tets' mode
            If ($mode -notlike 'Test') {
                Register-Event "INFO" "Attempting to move Computer Account to new OU via WebService"
                $computerMoved = Move-ComputerOU ($WPFtxtTargetDomain.text).Trim() ($WPFtxtComputerName.text).Trim() $WPFcmbTargetOU.SelectedValue
                # Move Computer Account to selected OU succeeded
                If ($computerMoved.result -like "true") {
                    $validation = $True
                    Register-Event "INFO" "Move Computer Account Validation PASSED"
                }
                # Move Computer Account to selected OU  failed
                Else {
                    $validation = $False
                    Register-Event "INFO" "Move Computer Account Validation FAILED"
                }
            }
            Else {Register-Event "INFO" "Bypassing Move OU as running in 'Test' mode"}
        }
        # CheckBox to move Computer Account is NOT set, warn user and allow them to remediate
        Else {
            Register-Event "WARNING" "Computer Account already exists in a different OU: $($arrPlatform.existingADComputerOU). Use Checkbox to Move via WebService or select this OU for deployment" 
            $validation = $False
        }
    }
    # Computer account doesn't exist or selected OU matches existing OU
    ElseIf ($validation -eq $True) {Register-Event "INFO" "Move Computer Account Validation PASSED"}

    # Refresh only, set/ update computer description, failure is non-critical, so no impact on validation - will warn user if this fails
    # For Bare Metal scenario's the value of WPFtxtComputerDescription is captured into TSVar computerDescription
    If ($arrPlatform.isRefresh -eq $True -and $validation -eq $True -and $WPFtxtComputerDescription.text -ne "") {
        # Do not want to update computer accoutn description when running in 'Test' mode
        If ($mode -notlike 'Test') {
            $computerDescription = Set-ComputerDescription ($WPFtxtTargetDomain.text).Trim() ($WPFtxtComputerName.text).Trim() ($WPFtxtComputerDescription.text).Trim()
            If ($computerDescription.result -like "true") {Register-Event "INFO" "Computer description set to $($WPFtxtComputerDescription.text)"}
            ElseIf ($computerDescription.result -like "false") {Register-Event "WARNING" "Failed to set computer description"}
        }
        Else {Register-Event "INFO" "Bypassing Set-ComputerDescription as running in 'Test' mode"}
    }

    # All validation steps passed
    If ($validation -eq $True) {
        Register-Event "INFO" "All pre-flight/ validation checks PASSED, proceeding with Task Seqeunce"
        Start-Build
    }
    # Validation did not succeed
    Else {
        Register-Event "INFO" "Pre-flight/ validation checks FAILED, cannot proceed with Task Seqeunce"
    }
}

Function Get-NetFrameworkVersion {
# Returns whether required .NET Framework (or newer) is installed, if not tiggers critical error
    [OutputType([PsObject])]
    Param ()
    try {
        $frameworkVersion = (Get-ItemProperty "hklm:SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\" "Release").Release
        If ($frameworkversion -ge "378389"){
            New-Object -TypeName PsObject -Property @{"supportedVersion" = $True}
            Register-Event "INFO" "Required .NET Framework detected, releaseID: $frameworkversion"
        }
        Else  {
            New-Object -TypeName PsObject -Property @{"supportedVersion" = $False}
            Register-Event "CRITICAL" "Required .NET Framework NOT detected, releaseID: $frameworkversion"
        }
    }
    catch {
        New-Object -TypeName PsObject -Property @{"supportedVersion" = $False}
        Register-Event "CRITICAL" "Required .NET Framework Version not available - this script requires. .NET 4.5 or newer" 
    }
}

Function Get-WebServiceOUExport {
# Uses Maik Koster's Deployment Web Service to set description for given Computer Account
    [CmdletBinding()]
    [OutputType([PsObject])]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $domain
    )
    try {
        Register-Event "INFO" "Using AD WebService to get Organisational Unit export for domain: $domain"
        [xml]$result = Invoke-WebRequest -Uri "$(($arrConfig.ADwebserviceURL).Replace('?WSDL',''))/GetOUs?Domain=$domain&ParentPath=&Level=$($arrConfig.ExportOULevel)"
        # Validate result actually contains OUs, if it does save to XML - invalid domain still returns some XML
        If ($result.ArrayOfOU.OU) {
            $xmlfilename = "$($domain.replace(".","-"))-OUs.xml"
            [xml]$result.Save("$PSScriptRoot\$xmlfilename")   
            Register-Event "INFO" "WebService saved OU export for $domain to $PSScriptRoot\$xmlfilename"   
        }
        Else {Register-Event "WARNING" "Unable to get Organisational Unit export for domain: $domain"   }
    }
    catch {
        Register-Event "WARNING" "Failed to get Organisational Unit export for domain: $domain" 
    }
}

Function Invoke-PreReqs {
# Loads essential COM objects, hides Task Sequence progress UI and connects to AD Web Service
    [CmdletBinding()]
    Param()
    $global:smsprovider = Connect-SMSTSEnv
    If ($smsprovider.available -eq $True) {$global:tsuiprovider = Connect-TSProgressUI}
    If ($tsuiprovider.available -eq $True) {Hide-TSProgressUI}
    If ($smsprovider.available -eq $True -and $tsuiprovider.available -eq $True) {$global:adwebservice = Connect-WebService}
}

Function New-Form {
# Generates UI (based on user-defined XAML) and runs initial population of UI components/ basic on-load validation of scenario
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [String]
        $OSDIsRefreshTS,
        [parameter(Mandatory=$true)]
        [String]
        $SMSTSPackageName        
    )
    If ($netframework.supportedVersion -eq $True) {
        # Parse and modify XAML to make it PowerShell XAML Compliant
        $inputXML = $inputXML -replace 'mc:Ignorable="d"','' -replace "x:N",'N'  -replace '^<Win.*', '<Window'
        [xml]$XAML = $inputXML
        $XmlNodeReader=(New-Object System.Xml.XmlNodeReader $XAML)
        try{
            $Form=[Windows.Markup.XamlReader]::Load( $XmlNodeReader )
            Register-Event "INFO" "Successfully parsed XAML"
            Add-Member -InputObject $arrConfig -MemberType NoteProperty -Name "XAMLparsed" -Value $True
        }
        catch{
            Register-Event "CRITICAL" "Failed to parse XAML, please check you have not included actions, such as onClick/ that .NET is installed. Occasionally this ia a WinPE init issue, try rebooting to WinPE." 
            Add-Member -InputObject $arrConfig -MemberType NoteProperty -Name "XAMLparsed" -Value $False
        }
    }
    Else {Add-Member -InputObject $arrConfig -MemberType NoteProperty -Name "XAMLparsed" -Value $False}
    
    If ($arrConfig.XAMLparsed -eq $True) {
        # Identify form elements, prefix with "WPF" to make identification simpler
        $XAML.SelectNodes("//*[@Name]") | ForEach-Object {Set-Variable -Name "WPF$($_.Name)" -Value $Form.FindName($_.Name)}
        # If ($arrConfig.debug -eq $True){Get-FormElement}
        # Prepare form elements/ actions
        $WPFbtnQuit.Add_Click({
            If ($smsprovider.available){$tsenv.value("OSDQuitTS") = "TRUE"}
            Register-Event "INFO" "User selected to Quit OSD Wizard"
            $Form.Close()
        })
        # Set Computer Account Reset to NO - making user have to do this, and thus think!
        $WPFrdoAccountResetNo.IsChecked = $true
        # Detect Task Sequence Scenario - i.e. Bare Metal/ Refresh/ Wipe and Re-Load
        If ($OSDIsRefreshTS -like "TRUE") {
            Register-Event "INFO" "Refresh scenario detected"
            Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "isRefresh" -Value $True       
            $existingADComputerDN = (Get-ComputerAttribute $arrPlatform.computerDomain $arrPlatform.computerName "distinguishedName").distinguishedName
            If ($existingADComputerDN) {$existingADComputerOU = $existingADComputerDN.Replace("CN=$($arrPlatform.computerName),", "")}
            Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "existingADComputerOU" -Value $existingADComputerOU
            Register-Event "INFO" "Existing Computer OU calculated as: $($arrPlatform.existingADComputerOU)"
            Enable-RefreshScenarioUI $arrPlatform.computerDomain $arrPlatform.computerName
        }
        Else {
            Register-Event "INFO" "Bare-metal scenario detected"
            Add-Member -InputObject $arrPlatform -MemberType NoteProperty -Name "isRefresh" -Value $False    
            $WPFchkMoveComputer.IsChecked = $false
        }
        # Get all location ID's/ Names
        $global:arrLocationIDName = Get-AllLocationIDName $arrConfig.locationsXML
        # Get client IPv4 gateway address, used to obtain actual location/ location settings
        $clientGateway = Get-ClientGateway
        # Get client LocationID and Name, based upon IPV4 gateway
        If ($clientGateway) {$arrClientLocationIDName = Get-ClientLocationIDName $clientGateway.IPv4address $arrConfig.locationsXML}
        # Populate location selection ComboBox, set selectedValue to detected location using ClientLocationName
        If ($arrClientLocationIDName) {Add-Location $arrClientLocationIDName.ClientLocationName}
        # Get all location settings for detected location
        If ($arrClientLocationIDName) {
            $global:arrLocationSettings = Get-LocationSettings $arrClientLocationIDName.ClientLocationID $arrConfig.locationsXML
        }
        # OSD Wizard has found location settings for detected location load/ populate WPF Form
        If ($arrLocationSettings) {
            # Populate OU selection ComboBox
            $JoinDomainXML = $arrLocationSettings.JoinDomain
            $JoinDomainXML = "$($JoinDomainXML.replace(".","-"))-OUs.xml"
            Get-OrganisationalUnits $JoinDomainXML "." 0
            # Populate OS list for Task Sequence here
            Get-AvailableOperatingSystem $arrConfig.tasksequenceXML $SMSTSPackageName
            # Confirm Platform is Valid for HTA to function
            If ($arrPlatform.chkPlatformValid) {
                If ($mode -like "Test") {Get-PlatformValid $arrPlatform.chkPlatformXML $arrPlatform.platformManufacturer $arrPlatform.platformModel $arrPlatform.platformBIOS $arrConfig.TSTplatformUEFIstate}
                Else {Get-PlatformValid $arrPlatform.chkPlatformXML $arrPlatform.platformManufacturer $arrPlatform.platformModel $arrPlatform.platformBIOS $tsenv.value("_SMSTSBootUEFI")}
            }
            # Auto-generate Computer name based upon locations XML file information
            If ($arrPlatform.isRefresh -eq $False -and $arrPlatform.validPlatform -eq $True) {New-ComputerName $arrPlatform.AssetTag $arrPlatform.platformType}
            # Post-render activities
            $form.add_contentrendered({
                $WPFcmbTargetLocation.Add_SelectionChanged({Select-Location}) ### problem here as triggers on load see https://stackoverflow.com/questions/2762042/xaml-combobox-selectionchanged-fires-onload
            })
            $WPFbtnContinue.Add_Click({Get-ValidationToProceed $arrPlatform.platformType})
            # Should be the last thing to happen under New-Form
            $Form.ShowDialog() | out-null
        }
        Else {Register-Event "CRITICAL" "OSD Wizard has been unable to detect client location. Unable to proceed."}

    }
}

<############################################################################################################################
Main Body
<############################################################################################################################>

# Clean-up variables from previous runsas these persist in session
If ($arrConfig) {Remove-Variable -Name "arrConfig" -Force -Scope global}
If ($arrPlatform) {Remove-Variable -Name "arrPlatform" -Force -Scope global}
If ($arrLocationIDName) {Remove-Variable "arrLocationIDName" -Force -Scope global}
If ($arrLocationSettings) {Remove-Variable "arrLocationSettings" -Force -Scope global}
If ($netFrameWork) {Remove-Variable "netFrameWork" -Force -Scope global}
If ($tsenv) {Remove-Variable -Name "tsenv" -Force -Scope global}
If ($ADWebS) {Remove-Variable -Name "ADWebS" -Force -Scope global}
If ($smsprovider) {Remove-Variable -Name "smsprovider" -Force -Scope global}
If ($tsuiprovider) {Remove-Variable -Name "tsuiprovider" -Force -Scope global}
If ($adwebservice) {Remove-Variable -Name "adwebservice" -Force -Scope global}

# Obtain script configuration and populate global array $arrConfig
$global:arrConfig = Get-Config "Config.xml"
If ($arrConfig) {
    # Output Config.xml settings to console/ log for troubleshooting purposes
    Register-Event "INFO" "================= START Configuration Settings ================="
    Register-Event "INFO" "Debug status: $($arrConfig.debug)"
    Register-Event "INFO" "Log file location: $($arrConfig.logfile)"
    Register-Event "INFO" "Locations XML file: $($arrConfig.locationsXML)"
    Register-Event "INFO" "Task Sequence XML file: $($arrConfig.tasksequenceXML)"
    Register-Event "INFO" "Same O/S Architecture Refresh: $($arrConfig.sameArchRefresh)"
    Register-Event "INFO" "Same OU during Refresh: $($arrConfig.sameOURefresh)"
    Register-Event "INFO" "Automatic computer/ hostname generation: $($arrConfig.generateComputerName)"
    Register-Event "INFO" "Web Service URL: $($arrConfig.ADwebserviceURL)"
    Register-Event "INFO" "Web Service Key: $($arrConfig.ADwebserviceKey)"
    Register-Event "INFO" "ExportOU levels: $($arrConfig.ExportOULevel)"
    Register-Event "INFO" "================= END Configuration Settings ==================="
    # Only display 'Test' mode related Config.xml settings if running in 'Test' mode
    If ($mode -like "Test") {
        Register-Event "INFO" "================= START Test Mode Settings ====================="
        Register-Event "INFO" "TEST isRefresh: $($arrConfig.TSTisRefresh)"
        Register-Event "INFO" "TEST Spoof Platform: $($arrConfig.TSTspoofPlatform)"
        Register-Event "INFO" "TEST Computer Name: $($arrConfig.TSTcomputerName)"
        Register-Event "INFO" "TEST Computer Domain:$($arrConfig.TSTcomputerDomain)"
        Register-Event "INFO" "TEST Platform Model: $($arrConfig.TSTplatformModel)"
        Register-Event "INFO" "TEST Platform Manufacturer: $($arrConfig.TSTplatformManufacturer)"
        Register-Event "INFO" "TEST Platform Model: $($arrConfig.TSTplatformModel)"
        Register-Event "INFO" "TEST Platform BIOS: $($arrConfig.TSTplatformBIOS)"
        Register-Event "INFO" "TEST Platform Asset Tag: $($arrConfig.TSTplatformAssetTag)"
        Register-Event "INFO" "TEST Platform UEFI State: $($arrConfig.TSTplatformUEFIstate)"
        Register-Event "INFO" "================= END Test Mode Settings ======================="
    }
    # Check .NET Framework version
    $global:netFrameWork = Get-NetFrameworkVersion
    # Obtain platform information and populate global array $arrPlatform
    $global:arrPlatform = Get-PlatformInfo
}
<############################################################################################################################
OSD Wizard Run-Time Mode Handler
<############################################################################################################################>
If ($arrConfig -and $netFrameWork.supportedVersion -eq $True -and $arrPlatform) {
    # Default mode - load and populate UI, perform initial validation of the detected scenario
    If ($mode -like "Default") {
        Register-Event "INFO" "Running OSD Wizard in 'Default' mode"    
        # Load/ check essentials, if these fail no point in continuing with Task Sequence
        Invoke-PreReqs
        # Load Graphical User Interface
        If ($smsprovider.available -eq $True -and $tsuiprovider.available -eq $True -and $adwebservice.available -eq $True) {
            If ($tsenv.value("OSDIsRefreshTS")) {New-Form $tsenv.value("OSDIsRefreshTS") $tsenv.value("_SMSTSPackageName")}
            Else {New-Form "FALSE" $tsenv.value("_SMSTSPackageName")}
        }
    }
    # SetDescription mode - read platform information and Task Sequence variable to set Active Directory Computer Account description attribute 
    ElseIf ($mode -like "SetDescription") {
        Register-Event "INFO" "Running OSD Wizard in 'SetDescription' mode"    
        # Load/ check essentials, if these fail no point in continuing with Task Sequence
        Invoke-PreReqs
        # Set Computer Account description if COM Object Available and machineDescription Task Sequence variable set
        If ($smsprovider.available -eq $True -and $tsenv.value("machineDescription") -ne "") {
            $adwebservice = Connect-WebService
            If ($adwebservice.available -eq $True) {
                Set-ComputerDescription $arrPlatform.computerDomain $arrPlatform.computerName $tsenv.value("machineDescription")
            }
        }
    }
    # ExportOU mode - export OUs for either supplied domain FQDN or, if not supplied, detected domain FQDN
    ElseIf ($mode -like "ExportOU") {
        Register-Event "INFO" "Running OSD Wizard in 'ExportOU' mode"    
        $adwebservice = Connect-WebService
        # If user has not supplied the FQDN of a domain, use platform detected domain for OU export
        If ($adwebservice.available -eq $True -and !$ouexportdomain) {
            Register-Event "INFO" "ExportOU using detected platform domain as 'ouexportdomain' parameter not supplied"
            Get-WebServiceOUExport $arrPlatform.computerDomain
        }
        # If user has supplied the FQDN of a domain, use this for OU export
        ElseIf ($adwebservice.available -eq $True -and $ouexportdomain) {
            Get-WebServiceOUExport $ouexportdomain
        }
        Else {Register-Event "WARNING" "ExportOU unable to proceed, please check command-line parameters"}  
    }
    # Test mode - use for testing of OSD Wizard outside of Task Sequence/ OSD scenario
    ElseIf ($mode -like "Test") {
        Register-Event "INFO" "Running OSD Wizard in 'Test' mode"
        # Connect to WebService - note that MoveOU/ SetDescription features are disabled in this mode
        $adwebservice = Connect-WebService
        # Spoof obtained platform information based on values defiend in Config.xml - requires TSTspoofPlatform setting to be 'True'
        If ($arrConfig.TSTspoofPlatform -eq $True) {
            Register-Event "WARNING" "Platform Spoofing Enabled in Config.xml"
            Register-Event "INFO" "================= START Platform Spoofing Information   ================="
            If ($arrConfig.TSTcomputerName) {
                Register-Event "INFO" "Spoofing Platform ComputerName: $($arrConfig.TSTcomputerName)"
                $arrPlatform.computerName = $arrConfig.TSTcomputerName
            }
            If ($arrConfig.TSTcomputerDomain) {
                Register-Event "INFO" "Spoofing Platform DomainName: $($arrConfig.TSTcomputerDomain)"
                $arrPlatform.computerDomain = $arrConfig.TSTcomputerDomain
            }
            If ($arrConfig.TSTplatformAssetTag) {
                Register-Event "INFO" "Spoofing Platform Asset Tag: $($arrConfig.TSTplatformAssetTag)"
                $arrplatform.assetTag = $arrConfig.TSTplatformAssetTag
            }
            If ($arrConfig.TSTplatformManufacturer) {
                Register-Event "INFO" "Spoofing Platform Manufacturer: $($arrConfig.TSTplatformManufacturer)"
                $arrPlatform.platformManufacturer = $arrConfig.TSTplatformManufacturer
            }
            If ($arrConfig.TSTplatformModel) {
                Register-Event "INFO" "Spoofing Platform Model: $($arrConfig.TSTplatformModel)"
                $arrPlatform.platformModel = $arrConfig.TSTplatformModel
            }
            If ($arrConfig.TSTplatformBIOS) {
                Register-Event "INFO" "Spoofing Platform BIOS: $($arrConfig.TSTplatformBIOS)"
                $arrPlatform.platformBIOS = $arrConfig.TSTplatformBIOS
            }
            Register-Event "INFO" "================= END Platform Spoofing Information  ================="
        }       
        New-Form $arrConfig.TSTisRefresh $arrConfig.TSTtsName
    }
}